#include "testMapeador.h"


testMapeador::testMapeador() {

}


testMapeador::~testMapeador() {

}


void testMapeador::testPipetoMessage() 
{
	HL7_24::ORU_R01 MSG;
	std::string pipe = "MSH|^~\&|||||20201001064756.444-0500||ORU^R01|301|T|2.3.1<CR>";
	try 
	{
		HL7_24::ORU_R01 MSG;
		STRINGS segs;
		pipe.append("PID|||M1015_00010 || John || 20091112 | M||||||<CR>");
		pipe.append("PV1 || I | ^^ICU&Bed5 & 3232241659 & 0 & 0|||||||||||||||A||||||||||||||||||||||||||20091201111211|<CR>");
		pipe.append("OBR||||Mindray Monitor|||20091203121631 |<CR>");
		pipe.append("OBX || NM | 52 ^ Height || 169.0||||||F|<CR>");
		split_to_vector(pipe, segs, "<CR>");
		while (!segs.empty())
		{
			ofLog() << "split de " << segs.front() << std::endl;
			segs.erase(segs.begin());
		}
		//pipeToSegment1(MSG.getPATIENT_RESULT(),segs);
	}
	catch (HL7Exception &hle) 
	{
		ofLog() << "error en el mapeo";
		throw hle;
	}
}


void testMapeador::testSplitMessage() 
{
	HL7_24::ORU_R01 MSG;
	std::string pipe = "<VT>MSH|^~\&|^EngineCaptura^|^^|^^|^^|||^|045dfbc5b1d4913e-4273f83e-4184449-156536a-1f7640e43c974d1af8538fea|P|2.3.4<CR>";
	try 
	{
		ofLog() << "inicio del test";
		HL7_24::ORU_R01 MSG;
		STRINGS segs;
		STRINGS seg;
		STRINGS datspli;
		pipe.append("<VT>PID|||PATIENT^196.76.0.3||NO_NAME||2020-09-06 12:20:55:86|NO_CAPTURE||^|^^^^^^^NO_ADDRES||^^^^^^^^NO_TEL<CR>");
		pipe.append("<VT>PV1||O|^||||^^NO_DOCTOR_CAP|||||||||||U||||||||||||||||||||^||||||2020-09-06 12:20:55:86<CR>");
		pipe.append("<VT>OBR||||^^^^Mindray_Monitor|||2020-09-06 12:20:55:86<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|AVF|0.060000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGECG1|}}}~~}}}}}}}}}}}}}}}}}}}}}}|{zyy~Ē~xyz{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~~~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGECG2|}}}~}}}}}}}}}}}}}}}}}}}}}|{zywvĨ°īŠ~vwyz||}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGECG3|}yuqptx}||||||F|||2020-09-06 12:20:39:814<CR>");
		split_to_vector(pipe, segs, "<CR><VT>");
		ofLog() << " pasos de el test"<<segs.size();
		HL7_24::PID * Pid = MSG.getPATIENT_RESULT()->
			getPATIENT()->
			getPID_4();
		HL7_24::PV1 * pv1 = MSG.getPATIENT_RESULT()->
			getPATIENT()->
			getVISIT()->
			getPV1_19();
		HL7_24::OBR * obr = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->
			getOBR_29();
		HL7_24::OBX* obx = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40(); // es un obx por cada parametros
		while (!segs.empty())
		{
			std::string cad = segs.front();
			ofLog() << "SE TRABAJARA EL SEGMENTO " << cad ;
			split_to_vector(cad, seg, '|');
			ofLog() << "split deL SEGMENTO "<< cad<<" tamaNIO "<< seg.size();
			if (cad.find("MSH") !=  std::string::npos)
			{
				ofLog() << "entre en el if " << seg.front();
				MSG.getMSH_1()->getMSH_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de 2" << seg.front();
				MSG.getMSH_1()->getMSH_2()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de 3" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				std::vector<std::string>::iterator subit = datspli.begin();
				MSG.getMSH_1()->getMSH_3()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_3()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_3()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de 4" << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_4()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_4()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_4()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de  5" << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_5()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_5()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_5()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_6()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_6()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_6()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_7()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_8()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_9()->getMessageStructure()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_10()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_11()->getProcessingID()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_12()->getVersionID()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
			}
			else if (seg.front().compare("PID")==0) 
			{
				seg.erase(seg.begin());
				ofLog()<<"SE ENTRA EN"<< seg.front();
				Pid->getPID_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_2()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() <<"__PID3__"<< seg.front();
				split_to_vector(seg.front(), datspli, '^');
				std::vector<std::string>::iterator it = datspli.begin();
				Pid->getPID_3()->getCX_1()->setData(*it); ++it;
				Pid->getPID_3()->getCX_2()->setData(*it); ++it;
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_4()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				Pid->getPID_5()->getFamilyName()->getFN_1()->setData
				(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_6()->getXPN_1()->getFN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_7()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_8()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_9()->getXPN_1()->getFN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_10()->getAlternateText()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog()<<"PID 11"<< seg.front();
				split_to_vector(seg.front(),datspli,'^');
				it = datspli.begin();
				Pid->getPID_11()->getXAD_1()->getSAD_1()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_2()->setData(*it);++it;
				Pid->getPID_11()->getXAD_3()->setData(*it);++it;
				Pid->getPID_11()->getXAD_4()->setData(*it);++it;
				Pid->getPID_11()->getXAD_5()->setData(*it);++it;
				Pid->getPID_11()->getXAD_6()->setData(*it);++it;
				Pid->getPID_11()->getXAD_7()->setData(*it);++it;
				Pid->getPID_11()->getXAD_8()->setData(*it);++it;
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_12()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog()<<"__pid_13_"<< seg.front();
				split_to_vector(seg.front(), datspli, '^');
				it = datspli.begin();
				Pid->getPID_13()->getXTN_1()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_2()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_3()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_4()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_5()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_6()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_7()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_8()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_9()->setData(*it); ++it;
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("PV1")==0) 
			{
			seg.erase(seg.begin());
			ofLog() << "se genero el front " << seg.front();
			pv1->getPV1_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "siguiente front " << seg.front();
			pv1->getPV1_2()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "siguiente front PV3" << seg.front();
			pv1->getPV1_3()->getBed()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_4()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_5()->getCX_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "siguiente front PV6__ " << seg.front();
			pv1->getPV1_6()->getPL_1()->setData(seg.front());
			seg.erase(seg.begin());
			split_to_vector(seg.front(), datspli, '^');
			ofLog() << "siguiente front PV7__" << seg.front();
			std::vector<std::string>::iterator it = datspli.begin();
			pv1->getPV1_7()->getXCN_1()->setData(*it); it++;
			pv1->getPV1_7()->getXCN_2()->getFN_1()->setData(*it); it++;
			pv1->getPV1_7()->getXCN_3()->setData(*it); it++;
			datspli.clear();
			seg.erase(seg.begin());
			ofLog() << "siguiente front PV8__" << seg.front();
			pv1->getPV1_8()->getXCN_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "siguiente front PV9__" << seg.front();
			pv1->getPV1_9()->getXCN_1()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_10()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_11()->getPL_1()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_12()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_13()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_14()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_15()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_16()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_17()->getXCN_1()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_18()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_19()->getCX_1()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_20()->getFC_1()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_21()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_22()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_23()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_24()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_25()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_26()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_27()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_28()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_29()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_30()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_31()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_32()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_33()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_34()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_35()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_36()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_37()->getDLD_1()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_38()->getAlternateText()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_39()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_40()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_41()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_42()->getPL_1()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_43()->getPL_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "siguiente front PV44__" << seg.front();
			pv1->getPV1_44()->getTS_1()->setData(seg.front());
			seg.erase(seg.begin());
			}
			else if (seg.front().compare("OBR")==0) 
			{
				seg.erase(seg.begin());
				ofLog() << "se obx__1" << seg.front();
				obr->getOBR_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__2" << seg.front();
				obr->getOBR_2()->getEI_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__2" << seg.front();
				obr->getOBR_3()->getEI_1()->setData(seg.front());
				seg.erase(seg.begin());
				std::vector<std::string>::iterator it = datspli.begin();
				split_to_vector(seg.front(),datspli,'^');
				obr->getOBR_4()->getCE_1()->setData(*it); ++it;
				obr->getOBR_4()->getCE_2()->setData(*it); ++it;
				obr->getOBR_4()->getCE_3()->setData(*it); ++it;
				obr->getOBR_4()->getCE_4()->setData(*it); ++it;
				obr->getOBR_4()->getAlternateText()->setData(*it); ++it;
				seg.erase(seg.begin());
				obr->getOBR_5()->setData(seg.front());
				seg.erase(seg.begin());
				obr->getOBR_6()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__7" << seg.front();
				obr->getOBR_7()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("OBX")==0) 
			{
			ofLog() << "primer segmento" << seg.front();
			seg.erase(seg.begin());
			ofLog() << "segundo segmento" << seg.front();
			obx->getOBX_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "tercer segmento" << seg.front();
			obx->getOBX_2()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "segmento split" << seg.front();
			split_to_vector(seg.front(), datspli, '^');
			std::vector<std::string>::iterator it=datspli.begin();
			obx->getOBX_3()->getCE_1()->setData(*it); ++it;
			obx->getOBX_3()->getCE_2()->setData(*it); ++it;//codigo para ecg
			seg.erase(seg.begin());
			datspli.clear();
			ofLog() << "segmento de validacion es " << seg.front();
			if (seg.front().compare("ECGECG1") == 0 || seg.front().compare("ECGECG3") == 0 || seg.front().compare("ECGECG2") == 0)
			{
				if (seg.front().compare("ECGECG2") == 0)
				{
					seg.erase(seg.begin());
					std::string hexi = seg.front();
					seg.erase(seg.begin());
					hexi.append(seg.front());
					seg.erase(seg.begin());
					hexi.append(seg.front());
					seg.erase(seg.begin());
					hexi.append(seg.front());
					ofLog() << "se interpretara los parametros 2" << hexi;
					while (!hexi.empty())
					{
						char cha = hexi.front();
						uint8_t arr = (uint8_t)cha;
						ofLog() << arr;
						hexi.erase(hexi.begin());
					}
				}else if (seg.front().compare("ECGECG1") == 0)
				 {
					seg.erase(seg.begin());
					std::string hexi = seg.front();
					seg.erase(seg.begin());
					hexi.append(seg.front());
					ofLog() << "se interpretara los parametros 1" << hexi;
					while (!hexi.empty())
					{
						char cha = hexi.front();
						uint8_t arr = (uint8_t)cha;
						ofLog() << arr;
						hexi.erase(hexi.begin());
					}
				}
				else 
				{
					seg.erase(seg.begin());
					std::string hexi = seg.front();
					seg.erase(seg.begin());
					ofLog() << "se interpretara los parametros 3" << hexi;
					while (!hexi.empty())
					{
						char cha = hexi.front();
						uint8_t arr = (uint8_t)cha;
						ofLog() << arr;
						hexi.erase(hexi.begin());
					}
				}

			}
			else 
			{
				ofLog() << " se almacena " << seg.front();
				obx->getObservationSubId()->setData(seg.front());//4
				
				seg.erase(seg.begin());
				ofLog() << " la data  " << seg.front();
				obx->getOBX_5()->setData(seg.front());
			}
			seg.erase(seg.begin());
			datspli.clear();

			obx->getOBX_6()->getCE_1()->setData(seg.front());
			seg.erase(seg.begin());
			obx->getOBX_7()->setData(seg.front());
			seg.erase(seg.begin());
			obx->getOBX_8()->setData(seg.front());
			seg.erase(seg.begin());
			obx->getOBX_9()->setData(seg.front());
			seg.erase(seg.begin());
			obx->getOBX_10()->setData(seg.front());
			seg.erase(seg.begin());
			obx->getOBX_11()->setData(seg.front());
			seg.erase(seg.begin());

			}

			

			seg.clear();
			segs.erase(segs.begin());
		}
	
	}
	catch (HL7Exception &ex)
	{
		ofLog() << "se genero un error";
		throw ex;
	}
}

void testMapeador::testPipetoSeg() 
{
	STRINGS vec;
	std::string pipe = "";
	pipe.append("PID|||M1015_00010 || John || 20091112 | M||||||\r");
	pipe.append("PV1 || I | ^^ICU&Bed5 & 3232241659 & 0 & 0|||||||||||||||A||||||||||||||||||||||||||20091201111211\r");
	pipe.append("OBR||||Mindray Monitor|||20091203121631 |\r");
	pipe.append("OBX || NM | 52 ^ Height || 169.0||||||F\r");
	split_to_vector(pipe, vec, '\r');
	
	while (!vec.empty()) 
	{
		ofLog() << "split de " << vec.front() <<std::endl;
		vec.erase(vec.begin());
	}
	

}


ECG testMapeador::testMapEcgObj() 
{
	ECG ecg;
	HL7_24::ORU_R01 MSG;
	std::string pipe = "<VT>MSH|^~\&|^EngineCaptura^|^^|^^|^^|||^|045dfbc5b1d4913e-4273f83e-4184449-156536a-1f7640e43c974d1af8538fea|P|2.3.4<CR>";
	try
	{
		ofLog() << "inicio del test";
		HL7_24::ORU_R01 MSG;
		STRINGS segs;
		STRINGS seg;
		STRINGS datspli;
		pipe.append("<VT>PID|||PATIENT^196.76.0.3||NO_NAME||2020-09-06 12:20:55:86|NO_CAPTURE||^|^^^^^^^NO_ADDRES||^^^^^^^^NO_TEL<CR>");
		pipe.append("<VT>PV1||O|^||||^^NO_DOCTOR_CAP|||||||||||U||||||||||||||||||||^||||||2020-09-06 12:20:55:86<CR>");
		pipe.append("<VT>OBR||||^^^^Mindray_Monitor|||2020-09-06 12:20:55:86<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|AVF|0.060000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|AVL|0.030000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|AVR|-0.090000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|CVP|0.000000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGFR|60.000000||||||F|||2020-09-06 12:20:39 :814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGI|0.080000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGII|0.100000||||||F|||2020-09-06 12:20:39 :814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGIII|0.020000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGV|0.040000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGECG1|}}}~~}}}}}}}}}}}}}}}}}}}}}}|{zyy~Ē~xyz{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~~~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGECG2|}}}~}}}}}}}}}}}}}}}}}}}}}|{zywvĨ°īŠ~vwyz||}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGECG3|}yuqptx}||||||F|||2020-09-06 12:20:39:814<CR>");
		split_to_vector(pipe, segs, "<CR><VT>");
		ofLog() << " pasos de el test" << segs.size();
		HL7_24::PID * Pid = MSG.getPATIENT_RESULT()->
			getPATIENT()->
			getPID_4();
		HL7_24::PV1 * pv1 = MSG.getPATIENT_RESULT()->
			getPATIENT()->
			getVISIT()->
			getPV1_19();
		HL7_24::OBR * obr = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->
			getOBR_29();
		HL7_24::OBX* obx = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40(); // es un obx por cada parametros
		while (!segs.empty())
		{
			std::string cad = segs.front();
			ofLog() << "SE TRABAJARA EL SEGMENTO " << cad;
			split_to_vector(cad, seg, '|');
			ofLog() << "split deL SEGMENTO " << cad << " tamaNIO " << seg.size();
			if (cad.find("MSH") != std::string::npos)
			{
				ofLog() << "entre en el if " << seg.front();
				MSG.getMSH_1()->getMSH_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de 2" << seg.front();
				MSG.getMSH_1()->getMSH_2()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de 3" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				std::vector<std::string>::iterator subit = datspli.begin();
				MSG.getMSH_1()->getMSH_3()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_3()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_3()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de 4" << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_4()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_4()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_4()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de  5" << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_5()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_5()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_5()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_6()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_6()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_6()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_7()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_8()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_9()->getMessageStructure()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_10()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_11()->getProcessingID()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_12()->getVersionID()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
			}
			else if (seg.front().compare("PID") == 0)
			{
				seg.erase(seg.begin());
				ofLog() << "SE ENTRA EN" << seg.front();
				Pid->getPID_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_2()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "__PID3__" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				
				std::vector<std::string>::iterator it = datspli.begin();
				Pid->getPID_3()->getCX_1()->setData(*it); ++it;
				Pid->getPID_3()->getCX_2()->setData(*it); ++it;
				ecg.setIp(Pid->getPID_3()->getCX_2()->getData());
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_4()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				Pid->getPID_5()->getFamilyName()->getFN_1()->setData
				(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_6()->getXPN_1()->getFN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_7()->getTS_1()->setData(seg.front());
				ecg.setdateSign(Pid->getPID_7()->getTS_1()->getData());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_8()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_9()->getXPN_1()->getFN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_10()->getAlternateText()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "PID 11" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				it = datspli.begin();
				Pid->getPID_11()->getXAD_1()->getSAD_1()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_2()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_3()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_4()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_5()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_6()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_7()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_8()->setData(*it); ++it;
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_12()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "__pid_13_" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				it = datspli.begin();
				Pid->getPID_13()->getXTN_1()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_2()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_3()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_4()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_5()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_6()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_7()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_8()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_9()->setData(*it); ++it;
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("PV1") == 0)
			{
				seg.erase(seg.begin());
				ofLog() << "se genero el front " << seg.front();
				pv1->getPV1_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front " << seg.front();
				pv1->getPV1_2()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV3" << seg.front();
				pv1->getPV1_3()->getBed()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_4()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_5()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV6__ " << seg.front();
				pv1->getPV1_6()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				split_to_vector(seg.front(), datspli, '^');
				ofLog() << "siguiente front PV7__" << seg.front();
				std::vector<std::string>::iterator it = datspli.begin();
				pv1->getPV1_7()->getXCN_1()->setData(*it); it++;
				pv1->getPV1_7()->getXCN_2()->getFN_1()->setData(*it); it++;
				pv1->getPV1_7()->getXCN_3()->setData(*it); it++;
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV8__" << seg.front();
				pv1->getPV1_8()->getXCN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV9__" << seg.front();
				pv1->getPV1_9()->getXCN_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_10()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_11()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_12()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_13()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_14()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_15()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_16()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_17()->getXCN_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_18()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_19()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_20()->getFC_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_21()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_22()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_23()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_24()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_25()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_26()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_27()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_28()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_29()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_30()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_31()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_32()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_33()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_34()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_35()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_36()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_37()->getDLD_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_38()->getAlternateText()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_39()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_40()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_41()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_42()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_43()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV44__" << seg.front();
				pv1->getPV1_44()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("OBR") == 0)
			{
				seg.erase(seg.begin());
				ofLog() << "se obx__1" << seg.front();
				obr->getOBR_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__2" << seg.front();
				obr->getOBR_2()->getEI_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__2" << seg.front();
				obr->getOBR_3()->getEI_1()->setData(seg.front());
				seg.erase(seg.begin());
				std::vector<std::string>::iterator it = datspli.begin();
				split_to_vector(seg.front(), datspli, '^');
				obr->getOBR_4()->getCE_1()->setData(*it); ++it;
				obr->getOBR_4()->getCE_2()->setData(*it); ++it;
				obr->getOBR_4()->getCE_3()->setData(*it); ++it;
				obr->getOBR_4()->getCE_4()->setData(*it); ++it;
				obr->getOBR_4()->getAlternateText()->setData(*it); ++it;
				seg.erase(seg.begin());
				obr->getOBR_5()->setData(seg.front());
				seg.erase(seg.begin());
				obr->getOBR_6()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__7" << seg.front();
				obr->getOBR_7()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("OBX") == 0)
			{
				ofLog() << "primer segmento" << seg.front();
				seg.erase(seg.begin());
				ofLog() << "segundo segmento" << seg.front();
				obx->getOBX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "tercer segmento" << seg.front();
				obx->getOBX_2()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "segmento split" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				std::vector<std::string>::iterator it = datspli.begin();
				obx->getOBX_3()->getCE_1()->setData(*it); ++it;
				obx->getOBX_3()->getCE_2()->setData(*it); ++it;//codigo para ecg
				ecg.setAcrom(obx->getOBX_3()->getCE_2()->getData());
				seg.erase(seg.begin());
				datspli.clear();
				ofLog() << "segmento de validacion es " << seg.front();
				if (seg.front().compare("ECGECG1") == 0 || seg.front().compare("ECGECG3") == 0 || seg.front().compare("ECGECG2") == 0)
				{
					if (seg.front().compare("ECGECG2") == 0)
					{
						seg.erase(seg.begin());
						std::string hexi = seg.front();
						std::vector<uint8_t> ecgv2;
						seg.erase(seg.begin());
						hexi.append(seg.front());
						seg.erase(seg.begin());
						hexi.append(seg.front());
						seg.erase(seg.begin());
						hexi.append(seg.front());
						ofLog() << "se interpretara los parametros 2" << hexi;
						while (!hexi.empty())
						{
							char cha = hexi.front();
							uint8_t arr = (uint8_t)cha;
							ecgv2.push_back(arr);
							hexi.erase(hexi.begin());
						}
						ecg.setECG2(ecgv2);
					}
					else if (seg.front().compare("ECGECG1") == 0)
					{
						seg.erase(seg.begin());
						std::string hexi = seg.front();
						std::vector<uint8_t> ecgv1;
						seg.erase(seg.begin());
						hexi.append(seg.front());
						ofLog() << "se interpretara los parametros 1" << hexi;
						while (!hexi.empty())
						{
							char cha = hexi.front();
							uint8_t arr = (uint8_t)cha;
							ecgv1.push_back(arr);
							hexi.erase(hexi.begin());
						}
						ecg.setECG1(ecgv1);
					}
					else
					{
						seg.erase(seg.begin());
						std::string hexi = seg.front();
						std::vector<uint8_t> ecgv3;
						seg.erase(seg.begin());
						ofLog() << "se interpretara los parametros 3" << hexi;
						while (!hexi.empty())
						{
							char cha = hexi.front();
							uint8_t arr = (uint8_t)cha;
							ecgv3.push_back(arr);
							hexi.erase(hexi.begin());
						}
						ecg.setECG3(ecgv3);
					}

				}
				else
				{
						ofLog() << " se almacena " << seg.front();
						obx->getObservationSubId()->setData(seg.front());//
					seg.erase(seg.begin());
					obx->getOBX_5()->setData(seg.front());
					ofLog() << "compara " << obx->getObservationSubId()->getData();
					if (std::string("AVF").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL AVF ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);
						ecg.setaVF(a);
						ofLog() << "se capturo el AVF " << ecg.getaVF();
					}else if (std::string("AVL").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL AVL ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);
						ecg.setaVL(a);
						ofLog() << "se capturo el AVL " << ecg.getaVL();
					}else if (std::string("AVR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL AVR ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);
						ecg.setaVR(a);
						ofLog() << "se capturo el AVR " << ecg.getaVR();
					}else if (std::string("CVP").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL CVP ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);
						ecg.setCVP(a);
						ofLog() << "se capturo el CVP " << ecg.getCVP();
					}else if (std::string("ECGI").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL I ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);
						ecg.setI(a);
						ofLog() << "se capturo el I " << ecg.getI();
					}else  if (std::string("ECGII").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL II ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);
						ecg.setII(a);
						ofLog() << "se capturo el II " << ecg.getII();
					}else if (std::string("ECGIII").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL III ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);
						ecg.setIII(a);
						ofLog() << "se capturo el III " << ecg.getIII();
					}
					else  if (std::string("ECGV").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL V ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);
						ecg.setV(a);
						ofLog() << "se capturo el V " << ecg.getV();
					}
					else if(std::string("ECGFR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL ECGFR ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);
						ecg.setFre_Card(a);
						ofLog() << "se capturo el ECGFR " << ecg.getFre_Card();
					}
					ofLog() << " la data  " << seg.front();
				}
				seg.erase(seg.begin());
				datspli.clear();

				obx->getOBX_6()->getCE_1()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_7()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_8()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_9()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_10()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_11()->setData(seg.front());
				seg.erase(seg.begin());

			}

			seg.clear();
			segs.erase(segs.begin());
		}

	}
	catch (HL7Exception &ex)
	{
		ofLogError()<< "se genero un error convirtiendo la cadena";
		throw ex;
	}

	return ecg;
}



Alarma testMapeador::testMapAlarmObj() 
{
	Alarma alrm;
	bool bandera = false;
	std::string pipe = "<VT>MSH|^~\&|^EngineCaptura^|^^|^^|^^|||^|045dfbc5b1d4913e-4273f83e-4184449-156536a-1f7640e43c974d1af8538fea|P|2.3.4<CR>";
	try
	{
		ofLog() << "inicio del test";
		HL7_24::ORU_R01 MSG;
		STRINGS mensajes;
		STRINGS segs;
		STRINGS seg;
		STRINGS datspli;
		pipe.append("<VT>PID|||PATIENT^196.76.0.3||NO_NAME||2020-09-06 12:20:55:86|NO_CAPTURE||^|^^^^^^^NO_ADDRES||^^^^^^^^NO_TEL<CR>");
		pipe.append("<VT>PV1||O|^||||^^NO_DOCTOR_CAP|||||||||||U||||||||||||||||||||^||||||2020-09-06 12:20:55:86<CR>");
		pipe.append("<VT>OBR||||^^^^Mindray_Monitor|||2020-09-06 12:20:55:86<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|AVF|0.060000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|AVL|0.030000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|AVR|-0.090000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|CVP|0.000000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGFR|60.000000||||||F|||2020-09-06 12:20:39 :814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGI|0.080000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGII|0.100000||||||F|||2020-09-06 12:20:39 :814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGIII|0.020000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGV|0.040000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGECG1|}}}~~}}}}}}}}}}}}}}}}}}}}}}|{zyy~Ē~xyz{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~~~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGECG2|}}}~}}}}}}}}}}}}}}}}}}}}}|{zywvĨ°īŠ~vwyz||}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|1^ECG|ECGECG3|}yuqptx}||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|6^ALRM|ALARMAVF|0.060000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|6^ALRM|ALARMAVL|0.030000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|6^ALRM|ALARMAVR|-0.090000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|6^ALRM|ALARMCVP|0.000000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGFR|60.000000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGI|0.080000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGII|0.100000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGIII|0.020000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGV|0.040000||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGECG1|}}}~~}}}}}}}}}}}}}}}}}}}}}}|{zyy~Ē~xyz{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~~~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGECG2|}}}~}}}}}}}}}}}}}}}}}}}}}|{zywvĨ°īŠ~vwyz||}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGECG3|}yuqptx}||||||F|||2020-09-06 12:20:39:814<CR>");
		pipe.append("<VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||2020-09-06 12:20:55:86<CR>");
		pipe.append("<VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||2020-09-06 12:20:55:86<CR>");
		pipe.append("<VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||2020-09-06 12:20:55:86<CR>");
		pipe.append("<VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||2020-09-06 12:20:55:86<CR>");

		split_to_vector(pipe, segs, "<CR><VT>");
		ofLog() << " pasos de el test" << segs.size();
		HL7_24::PID * Pid = MSG.getPATIENT_RESULT()->
			getPATIENT()->
			getPID_4();
		HL7_24::PV1 * pv1 = MSG.getPATIENT_RESULT()->
			getPATIENT()->
			getVISIT()->
			getPV1_19();
		HL7_24::OBR * obr = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->
			getOBR_29();
		HL7_24::OBX* obx = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40(); // es un obx por cada parametros
		while (!segs.empty())
		{
			std::string cad = segs.front();
			ofLog() << "SE TRABAJARA EL SEGMENTO " << cad;
			split_to_vector(cad, seg, '|');
			ofLog() << "split deL SEGMENTO " << cad << " tamaNIO " << seg.size();
			if (cad.find("MSH") != std::string::npos)
			{
				ofLog() << "entre en el if " << seg.front();
				MSG.getMSH_1()->getMSH_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de 2" << seg.front();
				MSG.getMSH_1()->getMSH_2()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de 3" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				std::vector<std::string>::iterator subit = datspli.begin();
				MSG.getMSH_1()->getMSH_3()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_3()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_3()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de 4" << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_4()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_4()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_4()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de  5" << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_5()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_5()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_5()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_6()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_6()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_6()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_7()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_8()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_9()->getMessageStructure()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_10()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_11()->getProcessingID()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_12()->getVersionID()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
			}
			else if (seg.front().compare("PID") == 0)
			{
				seg.erase(seg.begin());
				ofLog() << "SE ENTRA EN" << seg.front();
				Pid->getPID_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_2()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "__PID3__" << seg.front();
				split_to_vector(seg.front(), datspli, '^');

				std::vector<std::string>::iterator it = datspli.begin();
				Pid->getPID_3()->getCX_1()->setData(*it); ++it;
				Pid->getPID_3()->getCX_2()->setData(*it); ++it;
				alrm.setIp(Pid->getPID_3()->getCX_2()->getData());
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_4()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				Pid->getPID_5()->getFamilyName()->getFN_1()->setData
				(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_6()->getXPN_1()->getFN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_7()->getTS_1()->setData(seg.front());
				alrm.setdateSign(Pid->getPID_7()->getTS_1()->getData());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_8()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_9()->getXPN_1()->getFN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_10()->getAlternateText()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "PID 11" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				it = datspli.begin();
				Pid->getPID_11()->getXAD_1()->getSAD_1()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_2()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_3()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_4()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_5()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_6()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_7()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_8()->setData(*it); ++it;
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_12()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "__pid_13_" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				it = datspli.begin();
				Pid->getPID_13()->getXTN_1()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_2()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_3()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_4()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_5()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_6()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_7()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_8()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_9()->setData(*it); ++it;
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("PV1") == 0)
			{
				seg.erase(seg.begin());
				ofLog() << "se genero el front " << seg.front();
				pv1->getPV1_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front " << seg.front();
				pv1->getPV1_2()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV3" << seg.front();
				pv1->getPV1_3()->getBed()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_4()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_5()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV6__ " << seg.front();
				pv1->getPV1_6()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				split_to_vector(seg.front(), datspli, '^');
				ofLog() << "siguiente front PV7__" << seg.front();
				std::vector<std::string>::iterator it = datspli.begin();
				pv1->getPV1_7()->getXCN_1()->setData(*it); it++;
				pv1->getPV1_7()->getXCN_2()->getFN_1()->setData(*it); it++;
				pv1->getPV1_7()->getXCN_3()->setData(*it); it++;
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV8__" << seg.front();
				pv1->getPV1_8()->getXCN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV9__" << seg.front();
				pv1->getPV1_9()->getXCN_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_10()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_11()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_12()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_13()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_14()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_15()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_16()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_17()->getXCN_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_18()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_19()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_20()->getFC_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_21()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_22()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_23()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_24()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_25()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_26()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_27()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_28()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_29()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_30()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_31()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_32()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_33()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_34()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_35()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_36()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_37()->getDLD_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_38()->getAlternateText()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_39()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_40()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_41()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_42()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_43()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV44__" << seg.front();
				pv1->getPV1_44()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("OBR") == 0)
			{
				seg.erase(seg.begin());
				ofLog() << "se obx__1" << seg.front();
				obr->getOBR_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__2" << seg.front();
				obr->getOBR_2()->getEI_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__2" << seg.front();
				obr->getOBR_3()->getEI_1()->setData(seg.front());
				seg.erase(seg.begin());
				std::vector<std::string>::iterator it = datspli.begin();
				split_to_vector(seg.front(), datspli, '^');
				obr->getOBR_4()->getCE_1()->setData(*it); ++it;
				obr->getOBR_4()->getCE_2()->setData(*it); ++it;
				obr->getOBR_4()->getCE_3()->setData(*it); ++it;
				obr->getOBR_4()->getCE_4()->setData(*it); ++it;
				obr->getOBR_4()->getAlternateText()->setData(*it); ++it;
				seg.erase(seg.begin());
				obr->getOBR_5()->setData(seg.front());
				seg.erase(seg.begin());
				obr->getOBR_6()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__7" << seg.front();
				obr->getOBR_7()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("OBX") == 0)
			{
				ofLog() << "primer segmento" << seg.front();
				seg.erase(seg.begin());
				ofLog() << "segundo segmento" << seg.front();
				obx->getOBX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "tercer segmento" << seg.front();
				obx->getOBX_2()->setData(seg.front());
				if (std::string(obx->getOBX_2()->getData()).compare("CE")==0) {bandera = true;}
				seg.erase(seg.begin());
				ofLog() << "segmento split" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				std::vector<std::string>::iterator it = datspli.begin();
				obx->getOBX_3()->getCE_1()->setData(*it); ++it;
				obx->getOBX_3()->getCE_2()->setData(*it); ++it;//codigo para ecg
				alrm.setAcrom(obx->getOBX_3()->getCE_2()->getData());
				seg.erase(seg.begin());
				datspli.clear();
				ofLog() << "segmento de validacion es " << seg.front();
				if (seg.front().compare("ALARMECGECG1") == 0 || seg.front().compare("ALARMECGECG3") == 0 || seg.front().compare("ALARMECGECG2") == 0)
				{
					if (seg.front().compare("ALARMECGECG2") == 0)
					{
						seg.erase(seg.begin());
						std::string hexi = seg.front();
						std::vector<uint8_t> ecgv2;
						seg.erase(seg.begin());
						hexi.append(seg.front());
						seg.erase(seg.begin());
						hexi.append(seg.front());
						seg.erase(seg.begin());
						hexi.append(seg.front());
						ofLog() << "se interpretara los parametros 2" << hexi;
						while (!hexi.empty())
						{
							char cha = hexi.front();
							uint8_t arr = (uint8_t)cha;
							ecgv2.push_back(arr);
							hexi.erase(hexi.begin());
						}
						alrm.setECG2(ecgv2);
					}
					else if (seg.front().compare("ALARMECGECG1") == 0)
					{
						seg.erase(seg.begin());
						std::string hexi = seg.front();
						std::vector<uint8_t> ecgv1;
						seg.erase(seg.begin());
						hexi.append(seg.front());
						ofLog() << " se interpretara los parametros 1" << hexi;
						while (!hexi.empty())
						{
							char cha = hexi.front();
							uint8_t arr = (uint8_t)cha;
							ecgv1.push_back(arr);
							hexi.erase(hexi.begin());
						}
						alrm.setECG1(ecgv1);
					}
					else
					{
						seg.erase(seg.begin());
						std::string hexi = seg.front();
						std::vector<uint8_t> ecgv3;
						seg.erase(seg.begin());
						ofLog() << "se interpretara los parametros 3" << hexi;
						while (!hexi.empty())
						{
							char cha = hexi.front();
							uint8_t arr = (uint8_t)cha;
							ecgv3.push_back(arr);
							hexi.erase(hexi.begin());
						}
						alrm.setECG3(ecgv3);
					}

				}
				else
				{
					ofLog() << " se almacena " << seg.front();
					obx->getObservationSubId()->setData(seg.front());//
					seg.erase(seg.begin());
					obx->getOBX_5()->setData(seg.front());
					ofLog() << "compara " << obx->getObservationSubId()->getData();
					if (std::string("ALARMAVF").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL AVF ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);
						alrm.setaVF(a);
						ofLog() << "se capturo el AVF " << alrm.getaVF();
					}
					else if (std::string("ALARMAVL").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL AVL ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);
						alrm.setaVL(a);
						ofLog() << "se capturo el AVL " << alrm.getaVL();
					}
					else if (std::string("ALARMAVR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL AVR ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);
						alrm.setaVR(a);
						ofLog() << "se capturo el AVR " << alrm.getaVR();
					}
					else if (std::string("ALARMCVP").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL CVP ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);
						alrm.setCVP(a);
						ofLog() << "se capturo el CVP " << alrm.getCVP();
					}
					else if (std::string("ALARMECGI").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL I ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);
						alrm.setI(a);
						ofLog() << "se capturo el I " << alrm.getI();
					}
					else if (std::string("ALARMECGII").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL II ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);
						alrm.setII(a);
						ofLog() << "se capturo el II " << alrm.getII();
					}
					else if (std::string("ALARMECGIII").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL III ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);
						alrm.setIII(a);
						ofLog() << "se capturo el III " << alrm.getIII();
					}
					else if (std::string("ALARMECGV").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL V ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);
						alrm.setV(a);
						ofLog() << "se capturo el V " << alrm.getV();
					}
					else if (std::string("ALARMECGFR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL ECGFR ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);
						alrm.setFre_Card(a);
						ofLog() << "se capturo el ECGFR " << alrm.getFre_Card();
					}
					if (bandera) 
					{
						ofLog() << "ENTRE AL ALARM "<< obx->getOBX_5()->getData();
						size_t offst = 0;
						mensajes.push_back(obx->getOBX_5()->getData());
						ofLog() << "se capturo el ECGFR ";
						alrm.setMensajes(mensajes);

					}

					ofLog() << " la data  " << seg.front();
				}


				seg.erase(seg.begin());
				datspli.clear();

				obx->getOBX_6()->getCE_1()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_7()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_8()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_9()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_10()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_11()->setData(seg.front());
				seg.erase(seg.begin());

			}

			seg.clear();
			segs.erase(segs.begin());
		}

	}
	catch (HL7Exception &ex)
	{
		ofLogError() << "se genero un error convirtiendo la cadena";
		throw ex;
	}

	return alrm;
}



SigYellRed testMapeador::testMapSigYellRedArt()
{
	SigYellRed signalRedyell;
	ofLog() << "inicio del test";
	bool bandera = false;
	HL7_24::ORU_R01 MSG;
	STRINGS mensajes;
	STRINGS segs;
	STRINGS seg;
	STRINGS datspli;
	std::string pipe = "<VT>MSH|^~\&|^EngineCaptura^|^^|^^|^^|||^|045dfbc5b1d4913e-4273f83e-4184449-156536a-1f7640e43c974d1af8538fea|P|2.3.4<CR>";
	pipe.append("<VT>PID|||PATIENT^196.76.0.3||NO_NAME||2020-09-06 12:20:55:86|NO_CAPTURE||^|^^^^^^^NO_ADDRES||^^^^^^^^NO_TEL<CR>");
	pipe.append("<VT>PV1||O|^||||^^NO_DOCTOR_CAP|||||||||||U||||||||||||||||||||^||||||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBR||||^^^^Mindray_Monitor|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|AVF|0.060000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|AVL|0.030000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|AVR|-0.090000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|CVP|0.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGFR|60.000000||||||F|||2020-09-06 12:20:39 :814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGI|0.080000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGII|0.100000||||||F|||2020-09-06 12:20:39 :814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGIII|0.020000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGV|0.040000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGECG1|}}}~~}}}}}}}}}}}}}}}}}}}}}}|{zyy~Ē~xyz{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~~~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGECG2|}}}~}}}}}}}}}}}}}}}}}}}}}|{zywvĨ°īŠ~vwyz||}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGECG3|}yuqptx}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTMAX|120.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTMIN|90.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTPAR|75.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTSIGN|||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPMAX|25.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPMIN|14.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPPAR|9.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPSIGN|00090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000a000c000e000f00110012001400140015001600170018001900190019001a001a001a001a00190019001800180017001600160014001400130013001200120012001200120012001200120012001200120012001200110011001100100010000f000f000f000e000e000d000d000c000c000c000c000b000b000b000b000b000b000a000a000a000a000a000a000a00090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000909||||||F|||2020-09-06 12:20:39:814<CR>");


	pipe.append("<VT>OBX||NM|6^ALRM|ALARMAVF|0.060000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMAVL|0.030000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMAVR|-0.090000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMCVP|0.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGFR|60.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGI|0.080000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGII|0.100000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGIII|0.020000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGV|0.040000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGECG1|}}}~~}}}}}}}}}}}}}}}}}}}}}}|{zyy~Ē~xyz{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~~~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGECG2|}}}~}}}}}}}}}}}}}}}}}}}}}|{zywvĨ°īŠ~vwyz||}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGECG3|}yuqptx}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||2020-09-06 12:20:55:86<CR>");

	split_to_vector(pipe, segs, "<CR><VT>");
	try
	{
	ofLog() << " pasos de el test" << segs.size();
	HL7_24::PID * Pid = MSG.getPATIENT_RESULT()->
		getPATIENT()->
		getPID_4();
	HL7_24::PV1 * pv1 = MSG.getPATIENT_RESULT()->
		getPATIENT()->
		getVISIT()->
		getPV1_19();
	HL7_24::OBR * obr = MSG.getPATIENT_RESULT()->
		getORDER_OBSERVATION()->
		getOBR_29();
	HL7_24::OBX* obx = MSG.getPATIENT_RESULT()->
		getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40(); // es un obx por cada parametros
	while (!segs.empty())
	{
		std::string cad = segs.front();
		ofLog() << "SE TRABAJARA EL SEGMENTO " << cad;
		split_to_vector(cad, seg, '|');
		ofLog() << "split deL SEGMENTO " << cad << " tamaNIO " << seg.size();
		if (cad.find("MSH") != std::string::npos)
		{
			ofLog() << "entre en el if " << seg.front();
			MSG.getMSH_1()->getMSH_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "split de 2" << seg.front();
			MSG.getMSH_1()->getMSH_2()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "split de 3" << seg.front();
			split_to_vector(seg.front(), datspli, '^');
			std::vector<std::string>::iterator subit = datspli.begin();
			MSG.getMSH_1()->getMSH_3()->getHD_1()->setData(*subit); subit++;
			MSG.getMSH_1()->getMSH_3()->getHD_2()->setData(*subit); subit++;
			MSG.getMSH_1()->getMSH_3()->getHD_3()->setData(*subit); subit++;
			seg.erase(seg.begin());
			ofLog() << "split de 4" << seg.front();
			datspli.clear();
			split_to_vector(seg.front(), datspli, '^');
			subit = datspli.begin();
			MSG.getMSH_1()->getMSH_4()->getHD_1()->setData(*subit); subit++;
			MSG.getMSH_1()->getMSH_4()->getHD_2()->setData(*subit); subit++;
			MSG.getMSH_1()->getMSH_4()->getHD_3()->setData(*subit); subit++;
			seg.erase(seg.begin());
			ofLog() << "split de  5" << seg.front();
			datspli.clear();
			split_to_vector(seg.front(), datspli, '^');
			subit = datspli.begin();
			MSG.getMSH_1()->getMSH_5()->getHD_1()->setData(*subit); subit++;
			MSG.getMSH_1()->getMSH_5()->getHD_2()->setData(*subit); subit++;
			MSG.getMSH_1()->getMSH_5()->getHD_3()->setData(*subit); subit++;
			seg.erase(seg.begin());
			ofLog() << "split de " << seg.front();
			datspli.clear();
			split_to_vector(seg.front(), datspli, '^');
			subit = datspli.begin();
			MSG.getMSH_1()->getMSH_6()->getHD_1()->setData(*subit); subit++;
			MSG.getMSH_1()->getMSH_6()->getHD_2()->setData(*subit); subit++;
			MSG.getMSH_1()->getMSH_6()->getHD_3()->setData(*subit); subit++;
			seg.erase(seg.begin());
			ofLog() << "split de " << seg.front();
			MSG.getMSH_1()->getMSH_7()->getTS_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "split de " << seg.front();
			MSG.getMSH_1()->getMSH_8()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "split de " << seg.front();
			MSG.getMSH_1()->getMSH_9()->getMessageStructure()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "split de " << seg.front();
			MSG.getMSH_1()->getMSH_10()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "split de " << seg.front();
			MSG.getMSH_1()->getMSH_11()->getProcessingID()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "split de " << seg.front();
			MSG.getMSH_1()->getMSH_12()->getVersionID()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "split de " << seg.front();
		}
		else if (seg.front().compare("PID") == 0)
		{
			seg.erase(seg.begin());
			ofLog() << "SE ENTRA EN" << seg.front();
			Pid->getPID_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << seg.front();
			Pid->getPID_2()->getCX_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "__PID3__" << seg.front();
			split_to_vector(seg.front(), datspli, '^');

			std::vector<std::string>::iterator it = datspli.begin();
			Pid->getPID_3()->getCX_1()->setData(*it); ++it;
			Pid->getPID_3()->getCX_2()->setData(*it); ++it;
			signalRedyell.setIp(Pid->getPID_3()->getCX_2()->getData());
			datspli.clear();
			seg.erase(seg.begin());
			ofLog() << seg.front();
			Pid->getPID_4()->getCX_1()->setData(seg.front());
			seg.erase(seg.begin());
			Pid->getPID_5()->getFamilyName()->getFN_1()->setData
			(seg.front());
			seg.erase(seg.begin());
			ofLog() << seg.front();
			Pid->getPID_6()->getXPN_1()->getFN_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << seg.front();
			Pid->getPID_7()->getTS_1()->setData(seg.front());
			signalRedyell.setdateSign(Pid->getPID_7()->getTS_1()->getData());
			seg.erase(seg.begin());
			ofLog() << seg.front();
			Pid->getPID_8()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << seg.front();
			Pid->getPID_9()->getXPN_1()->getFN_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << seg.front();
			Pid->getPID_10()->getAlternateText()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "PID 11" << seg.front();
			split_to_vector(seg.front(), datspli, '^');
			it = datspli.begin();
			Pid->getPID_11()->getXAD_1()->getSAD_1()->setData(*it); ++it;
			Pid->getPID_11()->getXAD_2()->setData(*it); ++it;
			Pid->getPID_11()->getXAD_3()->setData(*it); ++it;
			Pid->getPID_11()->getXAD_4()->setData(*it); ++it;
			Pid->getPID_11()->getXAD_5()->setData(*it); ++it;
			Pid->getPID_11()->getXAD_6()->setData(*it); ++it;
			Pid->getPID_11()->getXAD_7()->setData(*it); ++it;
			Pid->getPID_11()->getXAD_8()->setData(*it); ++it;
			datspli.clear();
			seg.erase(seg.begin());
			ofLog() << seg.front();
			Pid->getPID_12()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "__pid_13_" << seg.front();
			split_to_vector(seg.front(), datspli, '^');
			it = datspli.begin();
			Pid->getPID_13()->getXTN_1()->setData(*it); ++it;
			Pid->getPID_13()->getXTN_2()->setData(*it); ++it;
			Pid->getPID_13()->getXTN_3()->setData(*it); ++it;
			Pid->getPID_13()->getXTN_4()->setData(*it); ++it;
			Pid->getPID_13()->getXTN_5()->setData(*it); ++it;
			Pid->getPID_13()->getXTN_6()->setData(*it); ++it;
			Pid->getPID_13()->getXTN_7()->setData(*it); ++it;
			Pid->getPID_13()->getXTN_8()->setData(*it); ++it;
			Pid->getPID_13()->getXTN_9()->setData(*it); ++it;
			seg.erase(seg.begin());
		}
		else if (seg.front().compare("PV1") == 0)
		{
			seg.erase(seg.begin());
			ofLog() << "se genero el front " << seg.front();
			pv1->getPV1_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "siguiente front " << seg.front();
			pv1->getPV1_2()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "siguiente front PV3" << seg.front();
			pv1->getPV1_3()->getBed()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_4()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_5()->getCX_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "siguiente front PV6__ " << seg.front();
			pv1->getPV1_6()->getPL_1()->setData(seg.front());
			seg.erase(seg.begin());
			split_to_vector(seg.front(), datspli, '^');
			ofLog() << "siguiente front PV7__" << seg.front();
			std::vector<std::string>::iterator it = datspli.begin();
			pv1->getPV1_7()->getXCN_1()->setData(*it); it++;
			pv1->getPV1_7()->getXCN_2()->getFN_1()->setData(*it); it++;
			pv1->getPV1_7()->getXCN_3()->setData(*it); it++;
			datspli.clear();
			seg.erase(seg.begin());
			ofLog() << "siguiente front PV8__" << seg.front();
			pv1->getPV1_8()->getXCN_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "siguiente front PV9__" << seg.front();
			pv1->getPV1_9()->getXCN_1()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_10()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_11()->getPL_1()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_12()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_13()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_14()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_15()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_16()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_17()->getXCN_1()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_18()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_19()->getCX_1()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_20()->getFC_1()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_21()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_22()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_23()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_24()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_25()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_26()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_27()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_28()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_29()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_30()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_31()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_32()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_33()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_34()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_35()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_36()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_37()->getDLD_1()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_38()->getAlternateText()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_39()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_40()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_41()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_42()->getPL_1()->setData(seg.front());
			seg.erase(seg.begin());
			pv1->getPV1_43()->getPL_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "siguiente front PV44__" << seg.front();
			pv1->getPV1_44()->getTS_1()->setData(seg.front());
			seg.erase(seg.begin());
		}
		else if (seg.front().compare("OBR") == 0)
		{
			seg.erase(seg.begin());
			ofLog() << "se obx__1" << seg.front();
			obr->getOBR_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "se obx__2" << seg.front();
			obr->getOBR_2()->getEI_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "se obx__2" << seg.front();
			obr->getOBR_3()->getEI_1()->setData(seg.front());
			seg.erase(seg.begin());
			std::vector<std::string>::iterator it = datspli.begin();
			split_to_vector(seg.front(), datspli, '^');
			obr->getOBR_4()->getCE_1()->setData(*it); ++it;
			obr->getOBR_4()->getCE_2()->setData(*it); ++it;
			obr->getOBR_4()->getCE_3()->setData(*it); ++it;
			obr->getOBR_4()->getCE_4()->setData(*it); ++it;
			obr->getOBR_4()->getAlternateText()->setData(*it); ++it;
			seg.erase(seg.begin());
			obr->getOBR_5()->setData(seg.front());
			seg.erase(seg.begin());
			obr->getOBR_6()->getTS_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "se obx__7" << seg.front();
			obr->getOBR_7()->getTS_1()->setData(seg.front());
			seg.erase(seg.begin());
		}
		else if (seg.front().compare("OBX") == 0)
		{
			ofLog() << "primer segmento" << seg.front();
			seg.erase(seg.begin());
			ofLog() << "segundo segmento" << seg.front();
			obx->getOBX_1()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "tercer segmento" << seg.front();
			if (std::string(obx->getOBX_2()->getData()).compare("CE") == 0) { bandera = true; }
			obx->getOBX_2()->setData(seg.front());
			seg.erase(seg.begin());
			ofLog() << "segmento split" << seg.front();
			split_to_vector(seg.front(), datspli, '^');
			std::vector<std::string>::iterator it = datspli.begin();
			obx->getOBX_3()->getCE_1()->setData(*it); ++it;
			obx->getOBX_3()->getCE_2()->setData(*it); ++it;//codigo para ecg
			signalRedyell.setAcrom(obx->getOBX_3()->getCE_2()->getData());
			seg.erase(seg.begin());
			datspli.clear();
			ofLog() << "segmento de validacion es " << seg.front();
			if (seg.front().compare("ALARMECGECG1") == 0 || seg.front().compare("ALARMECGECG3") == 0 || seg.front().compare("ALARMECGECG2") == 0)
			{
				if (seg.front().compare("ALARMECGECG2") == 0)
				{
					seg.erase(seg.begin());
					std::string hexi = seg.front();
					std::vector<uint8_t> ecgv2;
					seg.erase(seg.begin());
					hexi.append(seg.front());
					seg.erase(seg.begin());
					hexi.append(seg.front());
					seg.erase(seg.begin());
					hexi.append(seg.front());
					ofLog() << "se interpretara los parametros 2" << hexi;
					while (!hexi.empty())
					{
						char cha = hexi.front();
						uint8_t arr = (uint8_t)cha;
						ecgv2.push_back(arr);
						hexi.erase(hexi.begin());
					}
					
				}
				else if (seg.front().compare("ALARMECGECG1") == 0)
				{
					seg.erase(seg.begin());
					std::string hexi = seg.front();
					std::vector<uint8_t> ecgv1;
					seg.erase(seg.begin());
					hexi.append(seg.front());
					ofLog() << "se interpretara los parametros 1" << hexi;
					while (!hexi.empty())
					{
						char cha = hexi.front();
						uint8_t arr = (uint8_t)cha;
						ecgv1.push_back(arr);
						hexi.erase(hexi.begin());
					}
					
				}
				else
				{
					seg.erase(seg.begin());
					std::string hexi = seg.front();
					std::vector<uint8_t> ecgv3;
					seg.erase(seg.begin());
					ofLog() << "se interpretara los parametros 3" << hexi;
					while (!hexi.empty())
					{
						char cha = hexi.front();
						uint8_t arr = (uint8_t)cha;
						ecgv3.push_back(arr);
						hexi.erase(hexi.begin());
					}
					
				}

			}
			else
			{
				ofLog() << " se almacena " << seg.front();
				obx->getObservationSubId()->setData(seg.front());//
				seg.erase(seg.begin());
				obx->getOBX_5()->setData(seg.front());
				ofLog() << "compara " << obx->getObservationSubId()->getData();
				if (std::string("ALARMAVF").compare(obx->getObservationSubId()->getData()) == 0)
				{
					ofLog() << "ENTRE AL AVF ";
					size_t offst = 0;
					float a = std::stof(obx->getOBX_5()->getData(), &offst);
					
				}
				else if (std::string("ALARMAVL").compare(obx->getObservationSubId()->getData()) == 0)
				{
					ofLog() << "ENTRE AL AVL ";
					size_t offst = 0;
					float a = std::stof(obx->getOBX_5()->getData(), &offst);
					
				}
				else if (std::string("ALARMAVR").compare(obx->getObservationSubId()->getData()) == 0)
				{
					ofLog() << "ENTRE AL AVR ";
					size_t offst = 0;
					float a = std::stof(obx->getOBX_5()->getData(), &offst);
					
					ofLog() << "se capturo el AVR ";
				}
				else if (std::string("ALARMCVP").compare(obx->getObservationSubId()->getData()) == 0)
				{
					ofLog() << "ENTRE AL CVP ";
					size_t offst = 0;
					float a = std::stof(obx->getOBX_5()->getData(), &offst);
					
					ofLog() << "se capturo el CVP ";
				}
				else if (std::string("ALARMECGI").compare(obx->getObservationSubId()->getData()) == 0)
				{
					ofLog() << "ENTRE AL I ";
					size_t offst = 0;
					float a = std::stof(obx->getOBX_5()->getData(), &offst);
					
					ofLog() << "se capturo el I ";
				}
				else if (std::string("ALARMECGII").compare(obx->getObservationSubId()->getData()) == 0)
				{
					ofLog() << "ENTRE AL II ";
					size_t offst = 0;
					float a = std::stof(obx->getOBX_5()->getData(), &offst);
					
					ofLog() << "se capturo el II ";
				}
				else if (std::string("ALARMECGIII").compare(obx->getObservationSubId()->getData()) == 0)
				{
					ofLog() << "ENTRE AL III ";
					size_t offst = 0;
					float a = std::stof(obx->getOBX_5()->getData(), &offst);
					
					ofLog() << "se capturo el III ";
				}
				else if (std::string("ALARMECGV").compare(obx->getObservationSubId()->getData()) == 0)
				{
					ofLog() << "ENTRE AL V ";
					size_t offst = 0;
					float a = std::stof(obx->getOBX_5()->getData(), &offst);
					
					ofLog() << "se capturo el V ";
				}
				else if (std::string("ALARMECGFR").compare(obx->getObservationSubId()->getData()) == 0)
				{
					ofLog() << "ENTRE AL ECGFR ";
					size_t offst = 0;
					float a = std::stof(obx->getOBX_5()->getData(), &offst);
					
					ofLog() << "se capturo el ECGFR ";
				}
				else if (std::string("RJAMARTMAX").compare(obx->getObservationSubId()->getData()) == 0) 
				{
					ofLog() << "ENTRE RJAMARTMAX";
					size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						signalRedyell.setMax(a);
						ofLog() << "se capturo el RJA ";
				}
				else if (std::string("RJAMARTMIN").compare(obx->getObservationSubId()->getData()) == 0)
				{
					ofLog() << "ENTRE AL RJA RJAMARTMIN ";
					size_t offst = 0;
					float a = std::stod(obx->getOBX_5()->getData(), &offst);
					signalRedyell.setMin(a);
					ofLog() << "se capturo el RJA ";
				}
				else if (std::string("RJAMARTPAR").compare(obx->getObservationSubId()->getData()) == 0)
				{
					ofLog() << "ENTRE AL RJA  RJAMARTPAR";
					size_t offst = 0;
					float a = std::stod(obx->getOBX_5()->getData(), &offst);
					signalRedyell.setparentesis(a);
					ofLog() << "se capturo el RJA ";
				}
				else if (std::string("RJAMARTSIGN").compare(obx->getObservationSubId()->getData()) == 0)
				{
					ofLog() << "ENTRE AL RJA ";
					size_t offst = 0;
					//float a = std::stod(obx->getOBX_5()->getData(), &offst);

					ofLog() << "se capturo el RJA ";
				}
				else if (std::string("RJAMAPMAX").compare(obx->getObservationSubId()->getData()) == 0)
				{
					ofLog() << "ENTRE AL RJA AP";
					size_t offst = 0;
					float a = std::stod(obx->getOBX_5()->getData(), &offst);

					ofLog() << "se capturo el RJA ";
				}
				else if (std::string("RJAMAPMIN").compare(obx->getObservationSubId()->getData()) == 0)
				{
				ofLog() << "ENTRE AL RJA AP";
				size_t offst = 0;
				float a = std::stod(obx->getOBX_5()->getData(), &offst);

				ofLog() << "se capturo el RJA ";
				}
				else if (std::string("RJAMAPPAR").compare(obx->getObservationSubId()->getData()) == 0)
				{
				ofLog() << "ENTRE AL RJA AP ";
				size_t offst = 0;
				float a = std::stod(obx->getOBX_5()->getData(), &offst);

				ofLog() << "se capturo el RJA ";
				}
				else if (std::string("RJAMAPSIGN").compare(obx->getObservationSubId()->getData()) == 0)
				{
				ofLog() << "ENTRE AL RJA  AP";
				size_t offst = 0;
				//float a = std::stod(obx->getOBX_5()->getData(), &offst);

				ofLog() << "se capturo el RJA ";
				}
				if (bandera)
				{
					ofLog() << "ENTRE AL ALARM " << obx->getOBX_5()->getData();
					size_t offst = 0;
					mensajes.push_back(obx->getOBX_5()->getData());
					ofLog() << "se capturo el ECGFR ";
					

				}

				ofLog() << " la data  " << seg.front();
			}


			seg.erase(seg.begin());
			datspli.clear();

			obx->getOBX_6()->getCE_1()->setData(seg.front());
			seg.erase(seg.begin());
			obx->getOBX_7()->setData(seg.front());
			seg.erase(seg.begin());
			obx->getOBX_8()->setData(seg.front());
			seg.erase(seg.begin());
			obx->getOBX_9()->setData(seg.front());
			seg.erase(seg.begin());
			obx->getOBX_10()->setData(seg.front());
			seg.erase(seg.begin());
			obx->getOBX_11()->setData(seg.front());
			seg.erase(seg.begin());

		}

		seg.clear();
		segs.erase(segs.begin());
	}

}
	catch (HL7Exception &ex)
	{
	ofLogError() << "se genero un error convirtiendo la cadena";
	throw ex;
	}
	return signalRedyell;
}



SigYellRed testMapeador::testMapSigYellRedAP()
{
	SigYellRed signalRedyell;
	ofLog() << "inicio del test";
	bool bandera = false;
	HL7_24::ORU_R01 MSG;
	STRINGS mensajes;
	STRINGS segs;
	STRINGS seg;
	STRINGS datspli;
	std::string pipe = "<VT>MSH|^~\&|^EngineCaptura^|^^|^^|^^|||^|045dfbc5b1d4913e-4273f83e-4184449-156536a-1f7640e43c974d1af8538fea|P|2.3.4<CR>";
	pipe.append("<VT>PID|||PATIENT^196.76.0.3||NO_NAME||2020-09-06 12:20:55:86|NO_CAPTURE||^|^^^^^^^NO_ADDRES||^^^^^^^^NO_TEL<CR>");
	pipe.append("<VT>PV1||O|^||||^^NO_DOCTOR_CAP|||||||||||U||||||||||||||||||||^||||||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBR||||^^^^Mindray_Monitor|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|AVF|0.060000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|AVL|0.030000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|AVR|-0.090000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|CVP|0.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGFR|60.000000||||||F|||2020-09-06 12:20:39 :814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGI|0.080000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGII|0.100000||||||F|||2020-09-06 12:20:39 :814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGIII|0.020000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGV|0.040000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGECG1|}}}~~}}}}}}}}}}}}}}}}}}}}}}|{zyy~Ē~xyz{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~~~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGECG2|}}}~}}}}}}}}}}}}}}}}}}}}}|{zywvĨ°īŠ~vwyz||}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGECG3|}yuqptx}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|4^FR|FRSIG|bbbbbbbbbbbbbbbccccdddddeefffggghhhiiijjjkkllmmmnnoopppqqrrssttuuvvvwwxxyyy{{{{|}}~~~||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|4^FR|FRIMP|20.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTMAX|120.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTMIN|90.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTPAR|75.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTSIGN|||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPMAX|25.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPMIN|14.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPPAR|9.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPSIGN|00090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000a000c000e000f00110012001400140015001600170018001900190019001a001a001a001a00190019001800180017001600160014001400130013001200120012001200120012001200120012001200120012001200110011001100100010000f000f000f000e000e000d000d000c000c000c000c000b000b000b000b000b000b000a000a000a000a000a000a000a00090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000909||||||F|||2020-09-06 12:20:39:814<CR>");


	pipe.append("<VT>OBX||NM|6^ALRM|ALARMAVF|0.060000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMAVL|0.030000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMAVR|-0.090000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMCVP|0.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGFR|60.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGI|0.080000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGII|0.100000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGIII|0.020000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGV|0.040000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGECG1|}}}~~}}}}}}}}}}}}}}}}}}}}}}|{zyy~Ē~xyz{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~~~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGECG2|}}}~}}}}}}}}}}}}}}}}}}}}}|{zywvĨ°īŠ~vwyz||}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGECG3|}yuqptx}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||2020-09-06 12:20:55:86<CR>");

	split_to_vector(pipe, segs, "<CR><VT>");
	try
	{
		ofLog() << " pasos de el test" << segs.size();
		HL7_24::PID * Pid = MSG.getPATIENT_RESULT()->
			getPATIENT()->
			getPID_4();
		HL7_24::PV1 * pv1 = MSG.getPATIENT_RESULT()->
			getPATIENT()->
			getVISIT()->
			getPV1_19();
		HL7_24::OBR * obr = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->
			getOBR_29();
		HL7_24::OBX* obx = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40(); // es un obx por cada parametros
		while (!segs.empty())
		{
			std::string cad = segs.front();
			ofLog() << "SE TRABAJARA EL SEGMENTO " << cad;
			split_to_vector(cad, seg, '|');
			ofLog() << "split deL SEGMENTO " << cad << " tamaNIO " << seg.size();
			if (cad.find("MSH") != std::string::npos)
			{
				ofLog() << "entre en el if " << seg.front();
				MSG.getMSH_1()->getMSH_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de 2" << seg.front();
				MSG.getMSH_1()->getMSH_2()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de 3" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				std::vector<std::string>::iterator subit = datspli.begin();
				MSG.getMSH_1()->getMSH_3()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_3()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_3()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de 4" << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_4()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_4()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_4()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de  5" << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_5()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_5()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_5()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_6()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_6()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_6()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_7()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_8()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_9()->getMessageStructure()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_10()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_11()->getProcessingID()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_12()->getVersionID()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
			}
			else if (seg.front().compare("PID") == 0)
			{
				seg.erase(seg.begin());
				ofLog() << "SE ENTRA EN" << seg.front();
				Pid->getPID_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_2()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "__PID3__" << seg.front();
				split_to_vector(seg.front(), datspli, '^');

				std::vector<std::string>::iterator it = datspli.begin();
				Pid->getPID_3()->getCX_1()->setData(*it); ++it;
				Pid->getPID_3()->getCX_2()->setData(*it); ++it;
				signalRedyell.setIp(Pid->getPID_3()->getCX_2()->getData());
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_4()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				Pid->getPID_5()->getFamilyName()->getFN_1()->setData
				(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_6()->getXPN_1()->getFN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_7()->getTS_1()->setData(seg.front());
				signalRedyell.setdateSign(Pid->getPID_7()->getTS_1()->getData());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_8()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_9()->getXPN_1()->getFN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_10()->getAlternateText()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "PID 11" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				it = datspli.begin();
				Pid->getPID_11()->getXAD_1()->getSAD_1()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_2()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_3()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_4()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_5()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_6()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_7()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_8()->setData(*it); ++it;
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_12()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "__pid_13_" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				it = datspli.begin();
				Pid->getPID_13()->getXTN_1()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_2()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_3()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_4()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_5()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_6()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_7()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_8()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_9()->setData(*it); ++it;
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("PV1") == 0)
			{
				seg.erase(seg.begin());
				ofLog() << "se genero el front " << seg.front();
				pv1->getPV1_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front " << seg.front();
				pv1->getPV1_2()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV3" << seg.front();
				pv1->getPV1_3()->getBed()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_4()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_5()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV6__ " << seg.front();
				pv1->getPV1_6()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				split_to_vector(seg.front(), datspli, '^');
				ofLog() << "siguiente front PV7__" << seg.front();
				std::vector<std::string>::iterator it = datspli.begin();
				pv1->getPV1_7()->getXCN_1()->setData(*it); it++;
				pv1->getPV1_7()->getXCN_2()->getFN_1()->setData(*it); it++;
				pv1->getPV1_7()->getXCN_3()->setData(*it); it++;
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV8__" << seg.front();
				pv1->getPV1_8()->getXCN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV9__" << seg.front();
				pv1->getPV1_9()->getXCN_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_10()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_11()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_12()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_13()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_14()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_15()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_16()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_17()->getXCN_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_18()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_19()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_20()->getFC_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_21()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_22()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_23()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_24()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_25()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_26()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_27()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_28()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_29()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_30()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_31()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_32()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_33()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_34()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_35()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_36()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_37()->getDLD_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_38()->getAlternateText()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_39()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_40()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_41()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_42()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_43()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV44__" << seg.front();
				pv1->getPV1_44()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("OBR") == 0)
			{
				seg.erase(seg.begin());
				ofLog() << "se obx__1" << seg.front();
				obr->getOBR_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__2" << seg.front();
				obr->getOBR_2()->getEI_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__2" << seg.front();
				obr->getOBR_3()->getEI_1()->setData(seg.front());
				seg.erase(seg.begin());
				std::vector<std::string>::iterator it = datspli.begin();
				split_to_vector(seg.front(), datspli, '^');
				obr->getOBR_4()->getCE_1()->setData(*it); ++it;
				obr->getOBR_4()->getCE_2()->setData(*it); ++it;
				obr->getOBR_4()->getCE_3()->setData(*it); ++it;
				obr->getOBR_4()->getCE_4()->setData(*it); ++it;
				obr->getOBR_4()->getAlternateText()->setData(*it); ++it;
				seg.erase(seg.begin());
				obr->getOBR_5()->setData(seg.front());
				seg.erase(seg.begin());
				obr->getOBR_6()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__7" << seg.front();
				obr->getOBR_7()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("OBX") == 0)
			{
				ofLog() << "primer segmento" << seg.front();
				seg.erase(seg.begin());
				ofLog() << "segundo segmento" << seg.front();
				obx->getOBX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "tercer segmento" << seg.front();
				if (std::string(obx->getOBX_2()->getData()).compare("CE") == 0) { bandera = true; }
				obx->getOBX_2()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "segmento split" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				std::vector<std::string>::iterator it = datspli.begin();
				obx->getOBX_3()->getCE_1()->setData(*it); ++it;
				obx->getOBX_3()->getCE_2()->setData(*it); ++it;//codigo para ecg
				signalRedyell.setAcrom(obx->getOBX_3()->getCE_2()->getData());
				seg.erase(seg.begin());
				datspli.clear();
				ofLog() << "segmento de validacion es " << seg.front();
				if (seg.front().compare("ALARMECGECG1") == 0 || seg.front().compare("ALARMECGECG3") == 0 || seg.front().compare("ALARMECGECG2") == 0)
				{
					if (seg.front().compare("ALARMECGECG2") == 0)
					{
						seg.erase(seg.begin());
						std::string hexi = seg.front();
						std::vector<uint8_t> ecgv2;
						seg.erase(seg.begin());
						hexi.append(seg.front());
						seg.erase(seg.begin());
						hexi.append(seg.front());
						seg.erase(seg.begin());
						hexi.append(seg.front());
						ofLog() << "se interpretara los parametros 2" << hexi;
						while (!hexi.empty())
						{
							char cha = hexi.front();
							uint8_t arr = (uint8_t)cha;
							ecgv2.push_back(arr);
							hexi.erase(hexi.begin());
						}

					}
					else if (seg.front().compare("ALARMECGECG1") == 0)
					{
						seg.erase(seg.begin());
						std::string hexi = seg.front();
						std::vector<uint8_t> ecgv1;
						seg.erase(seg.begin());
						hexi.append(seg.front());
						ofLog() << "se interpretara los parametros 1" << hexi;
						while (!hexi.empty())
						{
							char cha = hexi.front();
							uint8_t arr = (uint8_t)cha;
							ecgv1.push_back(arr);
							hexi.erase(hexi.begin());
						}

					}
					else
					{
						seg.erase(seg.begin());
						std::string hexi = seg.front();
						std::vector<uint8_t> ecgv3;
						seg.erase(seg.begin());
						ofLog() << "se interpretara los parametros 3" << hexi;
						while (!hexi.empty())
						{
							char cha = hexi.front();
							uint8_t arr = (uint8_t)cha;
							ecgv3.push_back(arr);
							hexi.erase(hexi.begin());
						}

					}

				}
				else
				{
					ofLog() << " se almacena " << seg.front();
					obx->getObservationSubId()->setData(seg.front());//
					seg.erase(seg.begin());
					obx->getOBX_5()->setData(seg.front());
					ofLog() << "compara " << obx->getObservationSubId()->getData();
					if (std::string("ALARMAVF").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL AVF ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

					}
					else if (std::string("ALARMAVL").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL AVL ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

					}
					else if (std::string("ALARMAVR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL AVR ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el AVR ";
					}
					else if (std::string("ALARMCVP").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL CVP ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el CVP ";
					}
					else if (std::string("ALARMECGI").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL I ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el I ";
					}
					else if (std::string("ALARMECGII").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL II ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el II ";
					}
					else if (std::string("ALARMECGIII").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL III ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el III ";
					}
					else if (std::string("ALARMECGV").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL V ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el V ";
					}
					else if (std::string("ALARMECGFR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL ECGFR ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el ECGFR ";
					}
					else if (std::string("RJAMARTMAX").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE RJAMARTMAX";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						signalRedyell.setMax(a);
						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMARTMIN").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA RJAMARTMIN ";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						signalRedyell.setMin(a);
						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMARTPAR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA  RJAMARTPAR";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						signalRedyell.setparentesis(a);
						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMARTSIGN").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA ";
						size_t offst = 0;
						//float a = std::stod(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMAPMAX").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA AP";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						signalRedyell.setMax(a);
						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMAPMIN").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA AP";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						signalRedyell.setMin(a);
						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMAPPAR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA AP ";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						signalRedyell.setparentesis(a);
						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMAPSIGN").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA  AP";
						size_t offst = 0;
						//float a = std::stod(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el RJA ";
					}
					if (bandera)
					{
						ofLog() << "ENTRE AL ALARM " << obx->getOBX_5()->getData();
						size_t offst = 0;
						mensajes.push_back(obx->getOBX_5()->getData());
						ofLog() << "se capturo el ECGFR ";


					}

					ofLog() << " la data  " << seg.front();
				}


				seg.erase(seg.begin());
				datspli.clear();

				obx->getOBX_6()->getCE_1()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_7()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_8()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_9()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_10()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_11()->setData(seg.front());
				seg.erase(seg.begin());

			}

			seg.clear();
			segs.erase(segs.begin());
		}

	}
	catch (HL7Exception &ex)
	{
		ofLogError() << "se genero un error convirtiendo la cadena";
		throw ex;
	}
	return signalRedyell;
}



FrecResp testMapeador::testMapSigFrecResp() 
{
	FrecResp frecResp;
	ofLog() << "inicio del test";
	bool bandera = false;
	HL7_24::ORU_R01 MSG;
	STRINGS mensajes;
	STRINGS segs;
	STRINGS seg;
	STRINGS datspli;
	std::string pipe = "<VT>MSH|^~\&|^EngineCaptura^|^^|^^|^^|||^|045dfbc5b1d4913e-4273f83e-4184449-156536a-1f7640e43c974d1af8538fea|P|2.3.4<CR>";
	pipe.append("<VT>PID|||PATIENT^196.76.0.3||NO_NAME||2020-09-06 12:20:55:86|NO_CAPTURE||^|^^^^^^^NO_ADDRES||^^^^^^^^NO_TEL<CR>");
	pipe.append("<VT>PV1||O|^||||^^NO_DOCTOR_CAP|||||||||||U||||||||||||||||||||^||||||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBR||||^^^^Mindray_Monitor|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|AVF|0.060000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|AVL|0.030000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|AVR|-0.090000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|CVP|0.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGFR|60.000000||||||F|||2020-09-06 12:20:39 :814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGI|0.080000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGII|0.100000||||||F|||2020-09-06 12:20:39 :814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGIII|0.020000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGV|0.040000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGECG1|}}}~~}}}}}}}}}}}}}}}}}}}}}}|{zyy~Ē~xyz{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~~~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGECG2|}}}~}}}}}}}}}}}}}}}}}}}}}|{zywvĨ°īŠ~vwyz||}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGECG3|}yuqptx}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|4^FR|FRSIG|bbbbbbbbbbbbbbbccccdddddeefffggghhhiiijjjkkllmmmnnoopppqqrrssttuuvvvwwxxyyy{{{{|}}~~~||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|4^FR|FRIMP|20.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTMAX|120.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTMIN|90.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTPAR|75.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTSIGN|||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPMAX|25.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPMIN|14.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPPAR|9.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPSIGN|00090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000a000c000e000f00110012001400140015001600170018001900190019001a001a001a001a00190019001800180017001600160014001400130013001200120012001200120012001200120012001200120012001200110011001100100010000f000f000f000e000e000d000d000c000c000c000c000b000b000b000b000b000b000a000a000a000a000a000a000a00090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000909||||||F|||2020-09-06 12:20:39:814<CR>");


	pipe.append("<VT>OBX||NM|6^ALRM|ALARMAVF|0.060000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMAVL|0.030000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMAVR|-0.090000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMCVP|0.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGFR|60.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGI|0.080000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGII|0.100000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGIII|0.020000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGV|0.040000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGECG1|}}}~~}}}}}}}}}}}}}}}}}}}}}}|{zyy~Ē~xyz{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~~~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGECG2|}}}~}}}}}}}}}}}}}}}}}}}}}|{zywvĨ°īŠ~vwyz||}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGECG3|}yuqptx}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||2020-09-06 12:20:55:86<CR>");

	split_to_vector(pipe, segs, "<CR><VT>");
	try
	{
		ofLog() << " pasos de el test" << segs.size();
		HL7_24::PID * Pid = MSG.getPATIENT_RESULT()->
			getPATIENT()->
			getPID_4();
		HL7_24::PV1 * pv1 = MSG.getPATIENT_RESULT()->
			getPATIENT()->
			getVISIT()->
			getPV1_19();
		HL7_24::OBR * obr = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->
			getOBR_29();
		HL7_24::OBX* obx = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40(); // es un obx por cada parametros
		while (!segs.empty())
		{
			std::string cad = segs.front();
			ofLog() << "SE TRABAJARA EL SEGMENTO " << cad;
			split_to_vector(cad, seg, '|');
			ofLog() << "split deL SEGMENTO " << cad << " tamaNIO " << seg.size();
			if (cad.find("MSH") != std::string::npos)
			{
				ofLog() << "entre en el if " << seg.front();
				MSG.getMSH_1()->getMSH_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de 2" << seg.front();
				MSG.getMSH_1()->getMSH_2()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de 3" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				std::vector<std::string>::iterator subit = datspli.begin();
				MSG.getMSH_1()->getMSH_3()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_3()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_3()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de 4" << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_4()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_4()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_4()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de  5" << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_5()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_5()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_5()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_6()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_6()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_6()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_7()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_8()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_9()->getMessageStructure()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_10()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_11()->getProcessingID()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_12()->getVersionID()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
			}
			else if (seg.front().compare("PID") == 0)
			{
				seg.erase(seg.begin());
				ofLog() << "SE ENTRA EN" << seg.front();
				Pid->getPID_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_2()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "__PID3__" << seg.front();
				split_to_vector(seg.front(), datspli, '^');

				std::vector<std::string>::iterator it = datspli.begin();
				Pid->getPID_3()->getCX_1()->setData(*it); ++it;
				Pid->getPID_3()->getCX_2()->setData(*it); ++it;
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_4()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				Pid->getPID_5()->getFamilyName()->getFN_1()->setData
				(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_6()->getXPN_1()->getFN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_7()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_8()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_9()->getXPN_1()->getFN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_10()->getAlternateText()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "PID 11" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				it = datspli.begin();
				Pid->getPID_11()->getXAD_1()->getSAD_1()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_2()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_3()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_4()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_5()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_6()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_7()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_8()->setData(*it); ++it;
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_12()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "__pid_13_" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				it = datspli.begin();
				Pid->getPID_13()->getXTN_1()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_2()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_3()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_4()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_5()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_6()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_7()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_8()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_9()->setData(*it); ++it;
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("PV1") == 0)
			{
				seg.erase(seg.begin());
				ofLog() << "se genero el front " << seg.front();
				pv1->getPV1_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front " << seg.front();
				pv1->getPV1_2()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV3" << seg.front();
				pv1->getPV1_3()->getBed()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_4()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_5()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV6__ " << seg.front();
				pv1->getPV1_6()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				split_to_vector(seg.front(), datspli, '^');
				ofLog() << "siguiente front PV7__" << seg.front();
				std::vector<std::string>::iterator it = datspli.begin();
				pv1->getPV1_7()->getXCN_1()->setData(*it); it++;
				pv1->getPV1_7()->getXCN_2()->getFN_1()->setData(*it); it++;
				pv1->getPV1_7()->getXCN_3()->setData(*it); it++;
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV8__" << seg.front();
				pv1->getPV1_8()->getXCN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV9__" << seg.front();
				pv1->getPV1_9()->getXCN_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_10()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_11()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_12()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_13()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_14()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_15()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_16()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_17()->getXCN_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_18()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_19()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_20()->getFC_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_21()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_22()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_23()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_24()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_25()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_26()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_27()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_28()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_29()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_30()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_31()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_32()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_33()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_34()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_35()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_36()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_37()->getDLD_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_38()->getAlternateText()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_39()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_40()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_41()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_42()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_43()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV44__" << seg.front();
				pv1->getPV1_44()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("OBR") == 0)
			{
				seg.erase(seg.begin());
				ofLog() << "se obx__1" << seg.front();
				obr->getOBR_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__2" << seg.front();
				obr->getOBR_2()->getEI_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__2" << seg.front();
				obr->getOBR_3()->getEI_1()->setData(seg.front());
				seg.erase(seg.begin());
				std::vector<std::string>::iterator it = datspli.begin();
				split_to_vector(seg.front(), datspli, '^');
				obr->getOBR_4()->getCE_1()->setData(*it); ++it;
				obr->getOBR_4()->getCE_2()->setData(*it); ++it;
				obr->getOBR_4()->getCE_3()->setData(*it); ++it;
				obr->getOBR_4()->getCE_4()->setData(*it); ++it;
				obr->getOBR_4()->getAlternateText()->setData(*it); ++it;
				seg.erase(seg.begin());
				obr->getOBR_5()->setData(seg.front());
				seg.erase(seg.begin());
				obr->getOBR_6()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__7" << seg.front();
				obr->getOBR_7()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("OBX") == 0)
			{
				ofLog() << "primer segmento" << seg.front();
				seg.erase(seg.begin());
				ofLog() << "segundo segmento" << seg.front();
				obx->getOBX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "tercer segmento" << seg.front();
				if (std::string(obx->getOBX_2()->getData()).compare("CE") == 0) { bandera = true; }
				obx->getOBX_2()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "segmento split" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				std::vector<std::string>::iterator it = datspli.begin();
				obx->getOBX_3()->getCE_1()->setData(*it); ++it;
				obx->getOBX_3()->getCE_2()->setData(*it); ++it;//codigo para ecg
				
				seg.erase(seg.begin());
				datspli.clear();
				ofLog() << "segmento de validacion es " << seg.front();
				if (seg.front().compare("ALARMECGECG1") == 0 || seg.front().compare("ALARMECGECG3") == 0 || seg.front().compare("ALARMECGECG2") == 0)
				{
					if (seg.front().compare("ALARMECGECG2") == 0)
					{
						seg.erase(seg.begin());
						std::string hexi = seg.front();
						std::vector<uint8_t> ecgv2;
						seg.erase(seg.begin());
						hexi.append(seg.front());
						seg.erase(seg.begin());
						hexi.append(seg.front());
						seg.erase(seg.begin());
						hexi.append(seg.front());
						ofLog() << "se interpretara los parametros 2" << hexi;
						while (!hexi.empty())
						{
							char cha = hexi.front();
							uint8_t arr = (uint8_t)cha;
							ecgv2.push_back(arr);
							hexi.erase(hexi.begin());
						}

					}
					else if (seg.front().compare("ALARMECGECG1") == 0)
					{
						seg.erase(seg.begin());
						std::string hexi = seg.front();
						std::vector<uint8_t> ecgv1;
						seg.erase(seg.begin());
						hexi.append(seg.front());
						ofLog() << "se interpretara los parametros 1" << hexi;
						while (!hexi.empty())
						{
							char cha = hexi.front();
							uint8_t arr = (uint8_t)cha;
							ecgv1.push_back(arr);
							hexi.erase(hexi.begin());
						}

					}
					else
					{
						seg.erase(seg.begin());
						std::string hexi = seg.front();
						std::vector<uint8_t> ecgv3;
						seg.erase(seg.begin());
						ofLog() << "se interpretara los parametros 3" << hexi;
						while (!hexi.empty())
						{
							char cha = hexi.front();
							uint8_t arr = (uint8_t)cha;
							ecgv3.push_back(arr);
							hexi.erase(hexi.begin());
						}

					}

				}
				else
				{
					ofLog() << " se almacena " << seg.front();
					obx->getObservationSubId()->setData(seg.front());//
					seg.erase(seg.begin());
					obx->getOBX_5()->setData(seg.front());
					ofLog() << "compara " << obx->getObservationSubId()->getData();
					if (std::string("ALARMAVF").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL AVF ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

					}
					else if (std::string("ALARMAVL").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL AVL ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

					}
					else if (std::string("ALARMAVR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL AVR ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el AVR ";
					}
					else if (std::string("ALARMCVP").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL CVP ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el CVP ";
					}
					else if (std::string("ALARMECGI").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL I ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el I ";
					}
					else if (std::string("ALARMECGII").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL II ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el II ";
					}
					else if (std::string("ALARMECGIII").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL III ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el III ";
					}
					else if (std::string("ALARMECGV").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL V ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el V ";
					}
					else if (std::string("ALARMECGFR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL ECGFR ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el ECGFR ";
					}
					else if (std::string("RJAMARTMAX").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE RJAMARTMAX";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						
						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMARTMIN").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA RJAMARTMIN ";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						
						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMARTPAR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA  RJAMARTPAR";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						
						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMARTSIGN").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA ";
						size_t offst = 0;
						//float a = std::stod(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMAPMAX").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA AP";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						
						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMAPMIN").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA AP";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						
						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMAPPAR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA AP ";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMAPSIGN").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA  AP";
						size_t offst = 0;
						//float a = std::stod(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("FRSIG").compare(obx->getObservationSubId()->getData()) == 0) {}
					else if (std::string("FRIMP").compare(obx->getObservationSubId()->getData()) == 0) 
						{
						ofLog() << "ENTRE AL FRiMP  FRiMP";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						frecResp.setImpedancia(a);
						ofLog() << "se capturo el FRiMP ";
						}
					if (bandera)
					{
						ofLog() << "ENTRE AL ALARM " << obx->getOBX_5()->getData();
						size_t offst = 0;
						mensajes.push_back(obx->getOBX_5()->getData());
						ofLog() << "se capturo el ECGFR ";


					}

					ofLog() << " la data  " << seg.front();
				}


				seg.erase(seg.begin());
				datspli.clear();

				obx->getOBX_6()->getCE_1()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_7()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_8()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_9()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_10()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_11()->setData(seg.front());
				seg.erase(seg.begin());

			}

			seg.clear();
			segs.erase(segs.begin());
		}

	}
	catch (HL7Exception &ex)
	{
		ofLogError() << "se genero un error convirtiendo la cadena";
		throw ex;
	}
	return frecResp;
}



SPO2 testMapeador::testMapSigSPO2()
{
	SPO2 spo2;
	ofLog() << "inicio del test";
	bool bandera = false;
	HL7_24::ORU_R01 MSG;
	STRINGS mensajes;
	STRINGS segs;
	STRINGS seg;
	STRINGS datspli;
	std::string pipe = "<VT>MSH|^~\&|^EngineCaptura^|^^|^^|^^|||^|045dfbc5b1d4913e-4273f83e-4184449-156536a-1f7640e43c974d1af8538fea|P|2.3.4<CR>";
	pipe.append("<VT>PID|||PATIENT^196.76.0.3||NO_NAME||2020-09-06 12:20:55:86|NO_CAPTURE||^|^^^^^^^NO_ADDRES||^^^^^^^^NO_TEL<CR>");
	pipe.append("<VT>PV1||O|^||||^^NO_DOCTOR_CAP|||||||||||U||||||||||||||||||||^||||||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBR||||^^^^Mindray_Monitor|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|AVF|0.060000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|AVL|0.030000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|AVR|-0.090000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|CVP|0.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGFR|60.000000||||||F|||2020-09-06 12:20:39 :814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGI|0.080000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGII|0.100000||||||F|||2020-09-06 12:20:39 :814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGIII|0.020000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGV|0.040000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGECG1|}}}~~}}}}}}}}}}}}}}}}}}}}}}|{zyy~Ē~xyz{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~~~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGECG2|}}}~}}}}}}}}}}}}}}}}}}}}}|{zywvĨ°īŠ~vwyz||}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGECG3|}yuqptx}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|4^FR|FRSIG|bbbbbbbbbbbbbbbccccdddddeefffggghhhiiijjjkkllmmmnnoopppqqrrssttuuvvvwwxxyyy{{{{|}}~~~||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|4^FR|FRIMP|20.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTMAX|120.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTMIN|90.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTPAR|75.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTSIGN|||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPMAX|25.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPMIN|14.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPPAR|9.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPSIGN|00090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000a000c000e000f00110012001400140015001600170018001900190019001a001a001a001a00190019001800180017001600160014001400130013001200120012001200120012001200120012001200120012001200110011001100100010000f000f000f000e000e000d000d000c000c000c000c000b000b000b000b000b000b000a000a000a000a000a000a000a00090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000909||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|2^SPO2|SPO2FR|60.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|2^SPO2|SPO2DESC|98.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|2^SPO2|SPO2SIGN|030202020201010101000000000000000000000000000000000000000000010203040507090b0d0f111416181b1e202325272a2c2e313335383a3c3e40424446494a4c4e5052535456575858595a5a5a5a5a5959595958575655545352514f4e4c4a4947454442413f3d3c3a383735333231302f2d2c2b292827262423222120201f1f1f1f1e1e1e1e1d1d1d1d1d1d1e1e1e1f1f20202121212122222222222222222221212121202020201f1f1e1e1d1d1d1c1c1b1b1b1b1a1a191918181818181717161615151414131312121111111111101010100f0f0f0f0e0e0d0d0c0c0c0c0c0b0b0a0a0909090908080707060606060605050505040404040404040404||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|5^TEMP|TEMPT1|37.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|5^TEMP|TEMPT2|37.200001||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|5^TEMP|TEMPT3|0.200000||||||F|||2020-09-06 12:20:39:814<CR>");

	pipe.append("<VT>OBX||NM|6^ALRM|ALARMAVF|0.060000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMAVL|0.030000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMAVR|-0.090000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMCVP|0.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGFR|60.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGI|0.080000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGII|0.100000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGIII|0.020000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGV|0.040000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGECG1|}}}~~}}}}}}}}}}}}}}}}}}}}}}|{zyy~Ē~xyz{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~~~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGECG2|}}}~}}}}}}}}}}}}}}}}}}}}}|{zywvĨ°īŠ~vwyz||}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGECG3|}yuqptx}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||2020-09-06 12:20:55:86<CR>");

	split_to_vector(pipe, segs, "<CR><VT>");
	try
	{
		ofLog() << " pasos de el test" << segs.size();
		HL7_24::PID * Pid = MSG.getPATIENT_RESULT()->
			getPATIENT()->
			getPID_4();
		HL7_24::PV1 * pv1 = MSG.getPATIENT_RESULT()->
			getPATIENT()->
			getVISIT()->
			getPV1_19();
		HL7_24::OBR * obr = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->
			getOBR_29();
		HL7_24::OBX* obx = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40(); // es un obx por cada parametros
		while (!segs.empty())
		{
			std::string cad = segs.front();
			ofLog() << "SE TRABAJARA EL SEGMENTO " << cad;
			split_to_vector(cad, seg, '|');
			ofLog() << "split deL SEGMENTO " << cad << " tamaNIO " << seg.size();
			if (cad.find("MSH") != std::string::npos)
			{
				ofLog() << "entre en el if " << seg.front();
				MSG.getMSH_1()->getMSH_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de 2" << seg.front();
				MSG.getMSH_1()->getMSH_2()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de 3" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				std::vector<std::string>::iterator subit = datspli.begin();
				MSG.getMSH_1()->getMSH_3()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_3()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_3()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de 4" << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_4()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_4()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_4()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de  5" << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_5()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_5()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_5()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_6()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_6()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_6()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_7()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_8()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_9()->getMessageStructure()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_10()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_11()->getProcessingID()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_12()->getVersionID()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
			}
			else if (seg.front().compare("PID") == 0)
			{
				seg.erase(seg.begin());
				ofLog() << "SE ENTRA EN" << seg.front();
				Pid->getPID_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_2()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "__PID3__" << seg.front();
				split_to_vector(seg.front(), datspli, '^');

				std::vector<std::string>::iterator it = datspli.begin();
				Pid->getPID_3()->getCX_1()->setData(*it); ++it;
				Pid->getPID_3()->getCX_2()->setData(*it); ++it;
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_4()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				Pid->getPID_5()->getFamilyName()->getFN_1()->setData
				(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_6()->getXPN_1()->getFN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_7()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_8()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_9()->getXPN_1()->getFN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_10()->getAlternateText()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "PID 11" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				it = datspli.begin();
				Pid->getPID_11()->getXAD_1()->getSAD_1()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_2()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_3()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_4()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_5()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_6()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_7()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_8()->setData(*it); ++it;
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_12()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "__pid_13_" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				it = datspli.begin();
				Pid->getPID_13()->getXTN_1()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_2()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_3()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_4()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_5()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_6()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_7()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_8()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_9()->setData(*it); ++it;
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("PV1") == 0)
			{
				seg.erase(seg.begin());
				ofLog() << "se genero el front " << seg.front();
				pv1->getPV1_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front " << seg.front();
				pv1->getPV1_2()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV3" << seg.front();
				pv1->getPV1_3()->getBed()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_4()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_5()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV6__ " << seg.front();
				pv1->getPV1_6()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				split_to_vector(seg.front(), datspli, '^');
				ofLog() << "siguiente front PV7__" << seg.front();
				std::vector<std::string>::iterator it = datspli.begin();
				pv1->getPV1_7()->getXCN_1()->setData(*it); it++;
				pv1->getPV1_7()->getXCN_2()->getFN_1()->setData(*it); it++;
				pv1->getPV1_7()->getXCN_3()->setData(*it); it++;
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV8__" << seg.front();
				pv1->getPV1_8()->getXCN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV9__" << seg.front();
				pv1->getPV1_9()->getXCN_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_10()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_11()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_12()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_13()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_14()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_15()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_16()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_17()->getXCN_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_18()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_19()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_20()->getFC_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_21()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_22()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_23()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_24()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_25()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_26()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_27()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_28()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_29()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_30()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_31()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_32()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_33()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_34()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_35()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_36()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_37()->getDLD_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_38()->getAlternateText()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_39()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_40()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_41()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_42()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_43()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV44__" << seg.front();
				pv1->getPV1_44()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("OBR") == 0)
			{
				seg.erase(seg.begin());
				ofLog() << "se obx__1" << seg.front();
				obr->getOBR_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__2" << seg.front();
				obr->getOBR_2()->getEI_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__2" << seg.front();
				obr->getOBR_3()->getEI_1()->setData(seg.front());
				seg.erase(seg.begin());
				std::vector<std::string>::iterator it = datspli.begin();
				split_to_vector(seg.front(), datspli, '^');
				obr->getOBR_4()->getCE_1()->setData(*it); ++it;
				obr->getOBR_4()->getCE_2()->setData(*it); ++it;
				obr->getOBR_4()->getCE_3()->setData(*it); ++it;
				obr->getOBR_4()->getCE_4()->setData(*it); ++it;
				obr->getOBR_4()->getAlternateText()->setData(*it); ++it;
				seg.erase(seg.begin());
				obr->getOBR_5()->setData(seg.front());
				seg.erase(seg.begin());
				obr->getOBR_6()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__7" << seg.front();
				obr->getOBR_7()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("OBX") == 0)
			{
				ofLog() << "primer segmento" << seg.front();
				seg.erase(seg.begin());
				ofLog() << "segundo segmento" << seg.front();
				obx->getOBX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "tercer segmento" << seg.front();
				if (std::string(obx->getOBX_2()->getData()).compare("CE") == 0) { bandera = true; }
				obx->getOBX_2()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "segmento split" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				std::vector<std::string>::iterator it = datspli.begin();
				obx->getOBX_3()->getCE_1()->setData(*it); ++it;
				obx->getOBX_3()->getCE_2()->setData(*it); ++it;//codigo para ecg

				seg.erase(seg.begin());
				datspli.clear();
				ofLog() << "segmento de validacion es " << seg.front();
				if (seg.front().compare("ALARMECGECG1") == 0 || seg.front().compare("ALARMECGECG3") == 0 || seg.front().compare("ALARMECGECG2") == 0)
				{
					if (seg.front().compare("ALARMECGECG2") == 0)
					{
						seg.erase(seg.begin());
						std::string hexi = seg.front();
						std::vector<uint8_t> ecgv2;
						seg.erase(seg.begin());
						hexi.append(seg.front());
						seg.erase(seg.begin());
						hexi.append(seg.front());
						seg.erase(seg.begin());
						hexi.append(seg.front());
						ofLog() << "se interpretara los parametros 2" << hexi;
						while (!hexi.empty())
						{
							char cha = hexi.front();
							uint8_t arr = (uint8_t)cha;
							ecgv2.push_back(arr);
							hexi.erase(hexi.begin());
						}

					}
					else if (seg.front().compare("ALARMECGECG1") == 0)
					{
						seg.erase(seg.begin());
						std::string hexi = seg.front();
						std::vector<uint8_t> ecgv1;
						seg.erase(seg.begin());
						hexi.append(seg.front());
						ofLog() << "se interpretara los parametros 1" << hexi;
						while (!hexi.empty())
						{
							char cha = hexi.front();
							uint8_t arr = (uint8_t)cha;
							ecgv1.push_back(arr);
							hexi.erase(hexi.begin());
						}

					}
					else
					{
						seg.erase(seg.begin());
						std::string hexi = seg.front();
						std::vector<uint8_t> ecgv3;
						seg.erase(seg.begin());
						ofLog() << "se interpretara los parametros 3" << hexi;
						while (!hexi.empty())
						{
							char cha = hexi.front();
							uint8_t arr = (uint8_t)cha;
							ecgv3.push_back(arr);
							hexi.erase(hexi.begin());
						}

					}

				}
				else
				{
					ofLog() << " se almacena " << seg.front();
					obx->getObservationSubId()->setData(seg.front());//
					seg.erase(seg.begin());
					obx->getOBX_5()->setData(seg.front());
					ofLog() << "compara " << obx->getObservationSubId()->getData();
					if (std::string("ALARMAVF").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL AVF ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

					}
					else if (std::string("ALARMAVL").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL AVL ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

					}
					else if (std::string("ALARMAVR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL AVR ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el AVR ";
					}
					else if (std::string("ALARMCVP").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL CVP ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el CVP ";
					}
					else if (std::string("ALARMECGI").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL I ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el I ";
					}
					else if (std::string("ALARMECGII").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL II ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el II ";
					}
					else if (std::string("ALARMECGIII").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL III ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el III ";
					}
					else if (std::string("ALARMECGV").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL V ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el V ";
					}
					else if (std::string("ALARMECGFR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL ECGFR ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el ECGFR ";
					}
					else if (std::string("RJAMARTMAX").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE RJAMARTMAX";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMARTMIN").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA RJAMARTMIN ";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMARTPAR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA  RJAMARTPAR";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMARTSIGN").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA ";
						size_t offst = 0;
						//float a = std::stod(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMAPMAX").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA AP";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMAPMIN").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA AP";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMAPPAR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA AP ";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMAPSIGN").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA  AP";
						size_t offst = 0;
						//float a = std::stod(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("FRSIG").compare(obx->getObservationSubId()->getData()) == 0) {}
					else if (std::string("FRIMP").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL FRiMP  FRiMP";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						ofLog() << "se capturo el FRiMP ";
					}
					else if (std::string("SPO2FR").compare(obx->getObservationSubId()->getData()) == 0)
					{
					ofLog() << "ENTRE AL SPO2FR  SPO2FR";
					size_t offst = 0;
					float a = std::stod(obx->getOBX_5()->getData(), &offst);
					spo2.setFrec_encia(a);
					ofLog() << "se capturo el FRiMP ";
					}
					else if (std::string("SPO2DESC").compare(obx->getObservationSubId()->getData()) == 0)
					{
					ofLog() << "ENTRE AL SPO2FR  SPO2FR";
					size_t offst = 0;
					float a = std::stod(obx->getOBX_5()->getData(), &offst);
					spo2.setDesconocido(a);
					ofLog() << "se capturo el SPO2FR ";
					}
					if (bandera)
					{
						ofLog() << "ENTRE AL ALARM " << obx->getOBX_5()->getData();
						size_t offst = 0;
						mensajes.push_back(obx->getOBX_5()->getData());
						ofLog() << "se capturo el ECGFR ";


					}

					ofLog() << " la data  " << seg.front();
				}


				seg.erase(seg.begin());
				datspli.clear();

				obx->getOBX_6()->getCE_1()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_7()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_8()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_9()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_10()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_11()->setData(seg.front());
				seg.erase(seg.begin());

			}

			seg.clear();
			segs.erase(segs.begin());
		}

	}
	catch (HL7Exception &ex)
	{
		ofLogError() << "se genero un error convirtiendo la cadena";
		throw ex;
	}
	return spo2;
}


MapeadorHl7 testMapeador::testAllTestLoad()
{
	std::string lin = "<VT>MSH|^~\&|^EngineCaptura^|^^|^^|^^|||^|045dfbc5b1d4913e-4273f83e-4184449-156536a-1f7640e43c974d1af8538fea|P|2.3.4<CR>";
	lin.append("<VT>PID|||PATIENT^196.76.0.3||NO_NAME||2020-09-06 12:20:55:86|NO_CAPTURE||^|^^^^^^^NO_ADDRES||^^^^^^^^NO_TEL<CR>");
	lin.append("<VT>PV1||O|^||||^^NO_DOCTOR_CAP|||||||||||U||||||||||||||||||||^||||||2020-09-06 12:20:55:86<CR>");
	lin.append("<VT>OBR||||^^^^Mindray_Monitor|||2020-09-06 12:20:55:86<CR>");
	lin.append("<VT>OBX||NM|1^ECG|AVF|0.060000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|1^ECG|AVL|0.030000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|1^ECG|AVR|-0.090000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|1^ECG|CVP|0.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|1^ECG|ECGFR|60.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|1^ECG|ECGI|0.080000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|1^ECG|ECGII|0.100000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|1^ECG|ECGIII|0.020000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|1^ECG|ECGV|0.040000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|1^ECG|ECGECG1|}}}~~}}}}}}}}}}}}}}}}}}}}}}|{zyy~Ē~xyz{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~~~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|1^ECG|ECGECG2|}}}~}}}}}}}}}}}}}}}}}}}}}|{zywvĨ°īŠ~vwyz||}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|1^ECG|ECGECG3|}yuqptx}||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|4^FR|FRSIG|bbbbbbbbbbbbbbbccccdddddeefffggghhhiiijjjkkllmmmnnoopppqqrrssttuuvvvwwxxyyy{{{{|}}~~~||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|4^FR|FRIMP|20.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|3^RJAM|RJAMARTMAX|120.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|3^RJAM|RJAMARTMIN|90.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|3^RJAM|RJAMARTPAR|75.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|3^RJAM|RJAMARTSIGN|||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|3^RJAM|RJAMAPMAX|25.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|3^RJAM|RJAMAPMIN|14.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|3^RJAM|RJAMAPPAR|9.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|3^RJAM|RJAMAPSIGN|00090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000a000c000e000f00110012001400140015001600170018001900190019001a001a001a001a00190019001800180017001600160014001400130013001200120012001200120012001200120012001200120012001200110011001100100010000f000f000f000e000e000d000d000c000c000c000c000b000b000b000b000b000b000a000a000a000a000a000a000a00090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000909||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|2^SPO2|SPO2FR|60.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|2^SPO2|SPO2DESC|98.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|2^SPO2|SPO2SIGN|030202020201010101000000000000000000000000000000000000000000010203040507090b0d0f111416181b1e202325272a2c2e313335383a3c3e40424446494a4c4e5052535456575858595a5a5a5a5a5959595958575655545352514f4e4c4a4947454442413f3d3c3a383735333231302f2d2c2b292827262423222120201f1f1f1f1e1e1e1e1d1d1d1d1d1d1e1e1e1f1f20202121212122222222222222222221212121202020201f1f1e1e1d1d1d1c1c1b1b1b1b1a1a191918181818181717161615151414131312121111111111101010100f0f0f0f0e0e0d0d0c0c0c0c0c0b0b0a0a0909090908080707060606060605050505040404040404040404||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|5^TEMP|TEMPT1|37.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|5^TEMP|TEMPT2|37.200001||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|5^TEMP|TEMPT3|0.200000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|6^ALRM|ALARMAVF|0.060000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|6^ALRM|ALARMAVL|0.030000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|6^ALRM|ALARMAVR|-0.090000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|6^ALRM|ALARMCVP|0.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|6^ALRM|ALARMECGFR|60.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|6^ALRM|ALARMECGI|0.080000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|6^ALRM|ALARMECGII|0.100000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|6^ALRM|ALARMECGIII|0.020000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|6^ALRM|ALARMECGV|0.040000||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|6^ALRM|ALARMECGECG1|}}}~~}}}}}}}}}}}}}}}}}}}}}}|{zyy~Ē~xyz{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~~~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|6^ALRM|ALARMECGECG2|}}}~}}}}}}}}}}}}}}}}}}}}}|{zywvĨ°īŠ~vwyz||}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||NM|6^ALRM|ALARMECGECG3|}yuqptx}||||||F|||2020-09-06 12:20:39:814<CR>");
	lin.append("<VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||2020-09-06 12:20:55:86<CR>");
	lin.append("<VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||2020-09-06 12:20:55:86<CR>");
	lin.append("<VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||2020-09-06 12:20:55:86<CR>");
	lin.append("<VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||2020-09-06 12:20:55:86<CR>");
	MapeadorHl7 map;
	map.pipeToMessage(lin);
	ofLog() << "valor de SPO2FR " << map.getSpo2().getFrec_encia();
	return map;
}

Temperatura testMapeador::testMapSigTemp()
{
	Temperatura temp;
	ofLog() << "inicio del test";
	bool bandera = false;
	HL7_24::ORU_R01 MSG;
	STRINGS mensajes;
	STRINGS segs;
	STRINGS seg;
	STRINGS datspli;
	std::string pipe = "<VT>MSH|^~\&|^EngineCaptura^|^^|^^|^^|||^|045dfbc5b1d4913e-4273f83e-4184449-156536a-1f7640e43c974d1af8538fea|P|2.3.4<CR>";
	pipe.append("<VT>PID|||PATIENT^196.76.0.3||NO_NAME||2020-09-06 12:20:55:86|NO_CAPTURE||^|^^^^^^^NO_ADDRES||^^^^^^^^NO_TEL<CR>");
	pipe.append("<VT>PV1||O|^||||^^NO_DOCTOR_CAP|||||||||||U||||||||||||||||||||^||||||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBR||||^^^^Mindray_Monitor|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|AVF|0.060000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|AVL|0.030000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|AVR|-0.090000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|CVP|0.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGFR|60.000000||||||F|||2020-09-06 12:20:39 :814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGI|0.080000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGII|0.100000||||||F|||2020-09-06 12:20:39 :814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGIII|0.020000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGV|0.040000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGECG1|}}}~~}}}}}}}}}}}}}}}}}}}}}}|{zyy~Ē~xyz{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~~~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGECG2|}}}~}}}}}}}}}}}}}}}}}}}}}|{zywvĨ°īŠ~vwyz||}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|1^ECG|ECGECG3|}yuqptx}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|4^FR|FRSIG|bbbbbbbbbbbbbbbccccdddddeefffggghhhiiijjjkkllmmmnnoopppqqrrssttuuvvvwwxxyyy{{{{|}}~~~||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|4^FR|FRIMP|20.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTMAX|120.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTMIN|90.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTPAR|75.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMARTSIGN|||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPMAX|25.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPMIN|14.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPPAR|9.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|3^RJAM|RJAMAPSIGN|00090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000a000c000e000f00110012001400140015001600170018001900190019001a001a001a001a00190019001800180017001600160014001400130013001200120012001200120012001200120012001200120012001200110011001100100010000f000f000f000e000e000d000d000c000c000c000c000b000b000b000b000b000b000a000a000a000a000a000a000a00090009000900090009000900090009000900090009000900090009000900090009000900090009000900090009000909||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|2^SPO2|SPO2FR|60.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|2^SPO2|SPO2DESC|98.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|2^SPO2|SPO2SIGN|030202020201010101000000000000000000000000000000000000000000010203040507090b0d0f111416181b1e202325272a2c2e313335383a3c3e40424446494a4c4e5052535456575858595a5a5a5a5a5959595958575655545352514f4e4c4a4947454442413f3d3c3a383735333231302f2d2c2b292827262423222120201f1f1f1f1e1e1e1e1d1d1d1d1d1d1e1e1e1f1f20202121212122222222222222222221212121202020201f1f1e1e1d1d1d1c1c1b1b1b1b1a1a191918181818181717161615151414131312121111111111101010100f0f0f0f0e0e0d0d0c0c0c0c0c0b0b0a0a0909090908080707060606060605050505040404040404040404||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|5^TEMP|TEMPT1|37.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|5^TEMP|TEMPT2|37.200001||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|5^TEMP|TEMPT3|0.200000||||||F|||2020-09-06 12:20:39:814<CR>");

	pipe.append("<VT>OBX||NM|6^ALRM|ALARMAVF|0.060000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMAVL|0.030000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMAVR|-0.090000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMCVP|0.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGFR|60.000000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGI|0.080000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGII|0.100000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGIII|0.020000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGV|0.040000||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGECG1|}}}~~}}}}}}}}}}}}}}}}}}}}}}|{zyy~Ē~xyz{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~~~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGECG2|}}}~}}}}}}}}}}}}}}}}}}}}}|{zywvĨ°īŠ~vwyz||}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}~}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||NM|6^ALRM|ALARMECGECG3|}yuqptx}||||||F|||2020-09-06 12:20:39:814<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||2020-09-06 12:20:55:86<CR>");
	pipe.append("<VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||2020-09-06 12:20:55:86<CR>");

	split_to_vector(pipe, segs, "<CR><VT>");
	try
	{
		ofLog() << " pasos de el test" << segs.size();
		HL7_24::PID * Pid = MSG.getPATIENT_RESULT()->
			getPATIENT()->
			getPID_4();
		HL7_24::PV1 * pv1 = MSG.getPATIENT_RESULT()->
			getPATIENT()->
			getVISIT()->
			getPV1_19();
		HL7_24::OBR * obr = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->
			getOBR_29();
		HL7_24::OBX* obx = MSG.getPATIENT_RESULT()->
			getORDER_OBSERVATION()->getOBSERVATION()->getOBX_40(); // es un obx por cada parametros
		while (!segs.empty())
		{
			std::string cad = segs.front();
			ofLog() << "SE TRABAJARA EL SEGMENTO " << cad;
			split_to_vector(cad, seg, '|');
			ofLog() << "split deL SEGMENTO " << cad << " tamaNIO " << seg.size();
			if (cad.find("MSH") != std::string::npos)
			{
				ofLog() << "entre en el if " << seg.front();
				MSG.getMSH_1()->getMSH_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de 2" << seg.front();
				MSG.getMSH_1()->getMSH_2()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de 3" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				std::vector<std::string>::iterator subit = datspli.begin();
				MSG.getMSH_1()->getMSH_3()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_3()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_3()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de 4" << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_4()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_4()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_4()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de  5" << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_5()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_5()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_5()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				datspli.clear();
				split_to_vector(seg.front(), datspli, '^');
				subit = datspli.begin();
				MSG.getMSH_1()->getMSH_6()->getHD_1()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_6()->getHD_2()->setData(*subit); subit++;
				MSG.getMSH_1()->getMSH_6()->getHD_3()->setData(*subit); subit++;
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_7()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_8()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_9()->getMessageStructure()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_10()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_11()->getProcessingID()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
				MSG.getMSH_1()->getMSH_12()->getVersionID()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "split de " << seg.front();
			}
			else if (seg.front().compare("PID") == 0)
			{
				seg.erase(seg.begin());
				ofLog() << "SE ENTRA EN" << seg.front();
				Pid->getPID_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_2()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "__PID3__" << seg.front();
				split_to_vector(seg.front(), datspli, '^');

				std::vector<std::string>::iterator it = datspli.begin();
				Pid->getPID_3()->getCX_1()->setData(*it); ++it;
				Pid->getPID_3()->getCX_2()->setData(*it); ++it;
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_4()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				Pid->getPID_5()->getFamilyName()->getFN_1()->setData
				(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_6()->getXPN_1()->getFN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_7()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_8()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_9()->getXPN_1()->getFN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_10()->getAlternateText()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "PID 11" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				it = datspli.begin();
				Pid->getPID_11()->getXAD_1()->getSAD_1()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_2()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_3()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_4()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_5()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_6()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_7()->setData(*it); ++it;
				Pid->getPID_11()->getXAD_8()->setData(*it); ++it;
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << seg.front();
				Pid->getPID_12()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "__pid_13_" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				it = datspli.begin();
				Pid->getPID_13()->getXTN_1()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_2()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_3()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_4()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_5()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_6()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_7()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_8()->setData(*it); ++it;
				Pid->getPID_13()->getXTN_9()->setData(*it); ++it;
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("PV1") == 0)
			{
				seg.erase(seg.begin());
				ofLog() << "se genero el front " << seg.front();
				pv1->getPV1_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front " << seg.front();
				pv1->getPV1_2()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV3" << seg.front();
				pv1->getPV1_3()->getBed()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_4()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_5()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV6__ " << seg.front();
				pv1->getPV1_6()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				split_to_vector(seg.front(), datspli, '^');
				ofLog() << "siguiente front PV7__" << seg.front();
				std::vector<std::string>::iterator it = datspli.begin();
				pv1->getPV1_7()->getXCN_1()->setData(*it); it++;
				pv1->getPV1_7()->getXCN_2()->getFN_1()->setData(*it); it++;
				pv1->getPV1_7()->getXCN_3()->setData(*it); it++;
				datspli.clear();
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV8__" << seg.front();
				pv1->getPV1_8()->getXCN_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV9__" << seg.front();
				pv1->getPV1_9()->getXCN_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_10()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_11()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_12()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_13()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_14()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_15()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_16()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_17()->getXCN_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_18()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_19()->getCX_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_20()->getFC_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_21()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_22()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_23()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_24()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_25()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_26()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_27()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_28()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_29()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_30()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_31()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_32()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_33()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_34()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_35()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_36()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_37()->getDLD_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_38()->getAlternateText()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_39()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_40()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_41()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_42()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				pv1->getPV1_43()->getPL_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "siguiente front PV44__" << seg.front();
				pv1->getPV1_44()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("OBR") == 0)
			{
				seg.erase(seg.begin());
				ofLog() << "se obx__1" << seg.front();
				obr->getOBR_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__2" << seg.front();
				obr->getOBR_2()->getEI_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__2" << seg.front();
				obr->getOBR_3()->getEI_1()->setData(seg.front());
				seg.erase(seg.begin());
				std::vector<std::string>::iterator it = datspli.begin();
				split_to_vector(seg.front(), datspli, '^');
				obr->getOBR_4()->getCE_1()->setData(*it); ++it;
				obr->getOBR_4()->getCE_2()->setData(*it); ++it;
				obr->getOBR_4()->getCE_3()->setData(*it); ++it;
				obr->getOBR_4()->getCE_4()->setData(*it); ++it;
				obr->getOBR_4()->getAlternateText()->setData(*it); ++it;
				seg.erase(seg.begin());
				obr->getOBR_5()->setData(seg.front());
				seg.erase(seg.begin());
				obr->getOBR_6()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "se obx__7" << seg.front();
				obr->getOBR_7()->getTS_1()->setData(seg.front());
				seg.erase(seg.begin());
			}
			else if (seg.front().compare("OBX") == 0)
			{
				ofLog() << "primer segmento" << seg.front();
				seg.erase(seg.begin());
				ofLog() << "segundo segmento" << seg.front();
				obx->getOBX_1()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "tercer segmento" << seg.front();
				if (std::string(obx->getOBX_2()->getData()).compare("CE") == 0) { bandera = true; }
				obx->getOBX_2()->setData(seg.front());
				seg.erase(seg.begin());
				ofLog() << "segmento split" << seg.front();
				split_to_vector(seg.front(), datspli, '^');
				std::vector<std::string>::iterator it = datspli.begin();
				obx->getOBX_3()->getCE_1()->setData(*it); ++it;
				obx->getOBX_3()->getCE_2()->setData(*it); ++it;//codigo para ecg

				seg.erase(seg.begin());
				datspli.clear();
				ofLog() << "segmento de validacion es " << seg.front();
				if (seg.front().compare("ALARMECGECG1") == 0 || seg.front().compare("ALARMECGECG3") == 0 || seg.front().compare("ALARMECGECG2") == 0)
				{
					if (seg.front().compare("ALARMECGECG2") == 0)
					{
						seg.erase(seg.begin());
						std::string hexi = seg.front();
						std::vector<uint8_t> ecgv2;
						seg.erase(seg.begin());
						hexi.append(seg.front());
						seg.erase(seg.begin());
						hexi.append(seg.front());
						seg.erase(seg.begin());
						hexi.append(seg.front());
						ofLog() << "se interpretara los parametros 2" << hexi;
						while (!hexi.empty())
						{
							char cha = hexi.front();
							uint8_t arr = (uint8_t)cha;
							ecgv2.push_back(arr);
							hexi.erase(hexi.begin());
						}

					}
					else if (seg.front().compare("ALARMECGECG1") == 0)
					{
						seg.erase(seg.begin());
						std::string hexi = seg.front();
						std::vector<uint8_t> ecgv1;
						seg.erase(seg.begin());
						hexi.append(seg.front());
						ofLog() << "se interpretara los parametros 1" << hexi;
						while (!hexi.empty())
						{
							char cha = hexi.front();
							uint8_t arr = (uint8_t)cha;
							ecgv1.push_back(arr);
							hexi.erase(hexi.begin());
						}

					}
					else
					{
						seg.erase(seg.begin());
						std::string hexi = seg.front();
						std::vector<uint8_t> ecgv3;
						seg.erase(seg.begin());
						ofLog() << "se interpretara los parametros 3" << hexi;
						while (!hexi.empty())
						{
							char cha = hexi.front();
							uint8_t arr = (uint8_t)cha;
							ecgv3.push_back(arr);
							hexi.erase(hexi.begin());
						}

					}

				}
				else
				{
					ofLog() << " se almacena " << seg.front();
					obx->getObservationSubId()->setData(seg.front());//
					seg.erase(seg.begin());
					obx->getOBX_5()->setData(seg.front());
					ofLog() << "compara " << obx->getObservationSubId()->getData();
					if (std::string("ALARMAVF").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL AVF ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

					}
					else if (std::string("ALARMAVL").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL AVL ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

					}
					else if (std::string("ALARMAVR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL AVR ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el AVR ";
					}
					else if (std::string("ALARMCVP").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL CVP ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el CVP ";
					}
					else if (std::string("ALARMECGI").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL I ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el I ";
					}
					else if (std::string("ALARMECGII").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL II ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el II ";
					}
					else if (std::string("ALARMECGIII").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL III ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el III ";
					}
					else if (std::string("ALARMECGV").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL V ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el V ";
					}
					else if (std::string("ALARMECGFR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL ECGFR ";
						size_t offst = 0;
						float a = std::stof(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el ECGFR ";
					}
					else if (std::string("RJAMARTMAX").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE RJAMARTMAX";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMARTMIN").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA RJAMARTMIN ";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMARTPAR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA  RJAMARTPAR";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMARTSIGN").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA ";
						size_t offst = 0;
						//float a = std::stod(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMAPMAX").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA AP";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMAPMIN").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA AP";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMAPPAR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA AP ";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("RJAMAPSIGN").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL RJA  AP";
						size_t offst = 0;
						//float a = std::stod(obx->getOBX_5()->getData(), &offst);

						ofLog() << "se capturo el RJA ";
					}
					else if (std::string("FRSIG").compare(obx->getObservationSubId()->getData()) == 0) {}
					else if (std::string("FRIMP").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL FRiMP  FRiMP";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						ofLog() << "se capturo el FRiMP ";
					}
					else if (std::string("SPO2FR").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL SPO2FR  SPO2FR";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						ofLog() << "se capturo el FRiMP ";
					}
					else if (std::string("SPO2DESC").compare(obx->getObservationSubId()->getData()) == 0)
					{
						ofLog() << "ENTRE AL SPO2FR  SPO2FR";
						size_t offst = 0;
						float a = std::stod(obx->getOBX_5()->getData(), &offst);
						ofLog() << "se capturo el SPO2FR ";
					}
					else if (std::string("TEMPT1").compare(obx->getObservationSubId()->getData()) == 0)
					{
					ofLog() << "ENTRE AL TEMPT1  TEMPT1";
					size_t offst = 0;
					float a = std::stod(obx->getOBX_5()->getData(), &offst);
					temp.setT1(a);
					ofLog() << "se capturo el TEMPT1 ";
					}
					else if (std::string("TEMPT2").compare(obx->getObservationSubId()->getData()) == 0)
					{
					ofLog() << "ENTRE AL TEMPT2  TEMPT2";
					size_t offst = 0;
					float a = std::stod(obx->getOBX_5()->getData(), &offst);
					temp.setT2(a);
					ofLog() << "se capturo el TEMPT2 ";
					}
					else if (std::string("TEMPT3").compare(obx->getObservationSubId()->getData()) == 0)
					{
					ofLog() << "ENTRE AL TEMPT3  TEMPT3";
					size_t offst = 0;
					float a = std::stod(obx->getOBX_5()->getData(), &offst);
					temp.setT3(a);
					ofLog() << "se capturo el TEMPT3 ";
					}
					if (bandera)
					{
						ofLog() << "ENTRE AL ALARM " << obx->getOBX_5()->getData();
						size_t offst = 0;
						mensajes.push_back(obx->getOBX_5()->getData());
						ofLog() << "se capturo el ECGFR ";
					}

					ofLog() << " la data  " << seg.front();
				}


				seg.erase(seg.begin());
				datspli.clear();

				obx->getOBX_6()->getCE_1()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_7()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_8()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_9()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_10()->setData(seg.front());
				seg.erase(seg.begin());
				obx->getOBX_11()->setData(seg.front());
				seg.erase(seg.begin());

			}

			seg.clear();
			segs.erase(segs.begin());
		}

	}
	catch (HL7Exception &ex)
	{
		ofLogError() << "se genero un error convirtiendo la cadena";
		throw ex;
	}
	return temp;
}



TEST(testMapeado,testAlarmObj) 
{
	try {
		testMapeador test;
		Alarma al=test.testMapAlarmObj();
		ASSERT_TRUE(al.getMensajes().size()>0);
	}
	catch (HL7Exception &ex)
	{
		ofLog() << "se genero un exception" << ex.m_what;
		ofLog() << "salida " << ex.what();
		ASSERT_TRUE(false);
	}
}





TEST(testMapeado, testpipeSplit) 
{
	try 
	{
		testMapeador test;
		test.testPipetoSeg();
		ASSERT_TRUE(true);
	}
	catch (HL7Exception &ex)
	{
		ofLog() << "se genero un exception" << ex.m_what;
		ofLog() <<"salida "<<ex.what();
		ASSERT_TRUE(false);
	}
}


TEST(testMapeado, testpipeToMsg)
{
	try
	{
		testMapeador test;
		test.testPipetoMessage();
		ASSERT_TRUE(true);
	}
	catch (HL7Exception &ex)
	{
		ofLog() << "se genero un exception" << ex.m_what;
		ofLog() << "salida " << ex.what();
		ASSERT_TRUE(false);
	}

}


TEST(testMapeado,splitSeg)
{
	try 
	{
		testMapeador test;
		test.testSplitMessage();
		ASSERT_TRUE(true);
	}
	catch (HL7Exception &ex) 
	{
		ASSERT_TRUE(false);
	}
}


TEST(testMapeo,loadEcg) 
{
	try 
	{
		testMapeador test;
		ECG ecg;
		ecg=test.testMapEcgObj();
		ASSERT_TRUE(ecg.getECG1().size() > 0);
		ASSERT_TRUE(ecg.getECG2().size() > 0);
		ASSERT_TRUE(ecg.getECG3().size() > 0);
	}
	catch (HL7Exception ex) 
	{
		ofLog() << "se genero un excepcion";
	}


}

TEST(testMapeo, loadEcg1)
{
	try
	{
		testMapeador test;
		ECG ecg;
		ecg = test.testMapEcgObj();
		ofLog() << "E VALOR DE ECG" << ecg.getIp();
		ASSERT_EQ(ecg.getIp(), "196.76.0.3");
		
	}
	catch (HL7Exception ex)
	{
		ofLog() << "se genero un excepcion";
	}

}


TEST(testMapeo, SignaYellRedART)
{

	try 
	{
		testMapeador test;
		SigYellRed sig = test.testMapSigYellRedArt();
		ASSERT_TRUE(sig.getMax()== 120.0f);
		ASSERT_TRUE(sig.getMin() == 90.0f);
	}
	catch (HL7Exception &ex)
	{

	}
}


TEST(testMapeo, SignaYellRedAP)
{

	try
	{
		testMapeador test;
		SigYellRed sig = test.testMapSigYellRedAP();
		ASSERT_TRUE(sig.getMax() == 25.0f);
		ASSERT_TRUE(sig.getMin() == 14.0f);
	}
	catch (HL7Exception &ex)
	{

	}
}



TEST(testMapeo, SignaFrecResp)
{

	try
	{
		testMapeador test;
		FrecResp sig = test.testMapSigFrecResp();
		ASSERT_TRUE(sig.getImpedancia() == 20.0f);
	}
	catch (HL7Exception &ex)
	{

	}
}

TEST(testMapeo,SignaSpo2)
{
	try 
	{
		testMapeador test;
		SPO2 spo=test.testMapSigSPO2();
		ASSERT_TRUE(spo.getFrec_encia() == 60.0f);
		ASSERT_TRUE(spo.getDesconocido()== 98.0f);
	}
	catch (HL7Exception ex) 
	{
	
	}


}


TEST(testMapeo, SignaTemp)
{
	try
	{
		testMapeador test;
		Temperatura temp = test.testMapSigTemp();
		ASSERT_TRUE(temp.getT1() == 37.0f);
		ASSERT_TRUE(temp.getT2() == 37.2f);
		ASSERT_TRUE(temp.getT3() == 0.2f);
	}
	catch (HL7Exception ex)
	{

	}
}


TEST(testMapAll,testECG2) 
{
	try
	{
		MapeadorHl7 map;
		testMapeador test;
		map = test.testAllTestLoad();
		ASSERT_TRUE(map.getEcg().getaVF() == 0.06f);
		ASSERT_TRUE(map.getEcg().getaVL() == 0.03f);
		ASSERT_TRUE(map.getEcg().getCVP() == 0.0f );
		ASSERT_TRUE(map.getSignalAp().getMax() == 25.0f);
		ASSERT_TRUE(map.getSignalAp().getMin() == 14.0f);
		ASSERT_TRUE(map.getSignalAp().getparentesis() == 9.0f);
	}
	catch (HL7Exception ex)
	{


	}
}


TEST(testMapAll, testECG2Sig)
{
	try
	{
		MapeadorHl7 map;
		testMapeador test;
		map = test.testAllTestLoad();
		ASSERT_TRUE(map.getEcg().getECG1().size() > 0);
		ASSERT_TRUE(map.getEcg().getECG2().size() > 0);
		ASSERT_TRUE(map.getEcg().getECG3().size() > 0);
	}
	catch (HL7Exception ex)
	{


	}
}

TEST(testMapAll, testFrecResp)
{
	try
	{
		MapeadorHl7 map;
		testMapeador test;
		map = test.testAllTestLoad();
		ASSERT_TRUE(map.getFrecResp().getImpedancia() != 45.4f);
		ASSERT_TRUE(map.getFrecResp().getsig().size() > 0);
	}
	catch (HL7Exception ex)
	{


	}
}


TEST(testMapAll, testSPO2)
{
	try
	{
		MapeadorHl7 map;
		testMapeador test;
		map = test.testAllTestLoad();
		ASSERT_TRUE(map.getSpo2().getFrec_encia() == 60.0f);
		ASSERT_TRUE(map.getSpo2().getDesconocido() == 98.0f);
		ASSERT_TRUE(map.getSpo2().getSinga().size() > 0);
	}
	catch (HL7Exception ex)
	{


	}
}

TEST(testMapAll, testTEMP)
{
	try
	{
		MapeadorHl7 map;
		testMapeador test;
		map = test.testAllTestLoad();
		ASSERT_TRUE(map.getTemp().getT1() == 37.0f);
		ASSERT_TRUE(map.getTemp().getT2() == 37.2f);
		ASSERT_TRUE(map.getTemp().getT3() == 0.2f);
	}
	catch (HL7Exception ex)
	{


	}
}