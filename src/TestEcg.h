#if !defined(_TESTECG_)
#define _TESTECG_
#pragma once
#include "../Include/ECG.h"
#include "gtest/gtest.h"

class TestEcg
{

	float pram;

	public:

	TestEcg();

	~TestEcg();

	float getpram();

	void setpram(float);

	float testloadParam( int);

};
#endif
