#include "testSPO2.h"


testSPO2::testSPO2(){

}

testSPO2::~testSPO2()
{

}


float testSPO2::testParamSPO2(int para) 
{
	SPO2 spo;
	switch (para) 
	{
	case 0:
		spo.setDesconocido(this->param);
		return spo.getDesconocido();
	break;

	case 1:
		spo.setFrec_encia(this->param);
		return spo.getFrec_encia();
	break;

	}
}


TEST(spo2loadDesconocido,testLoadDesconocido)
{
	testSPO2 test;
	test.param = 15.7f;
	ASSERT_EQ(15.7f, test.testParamSPO2(0));
}


TEST(spo2loadFrecCardi, testLoadFrecCardi)
{
	testSPO2 test;
	test.param = 18.9f;
	ASSERT_EQ(18.9f, test.testParamSPO2(1));
}