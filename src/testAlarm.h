#if !defined(_TESTALARM_)
#define _TESTALARM_
#pragma once
#include "../Include/Alarma.h"
#include "gtest/gtest.h"
class testAlarm
{

	public:
	testAlarm();
	~testAlarm();

	float testInherent();

	std::vector<std::string> testMensage();


};


#endif
