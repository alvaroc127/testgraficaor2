#include "testAlarm.h"



testAlarm::testAlarm() 
{

}
testAlarm::~testAlarm() 
{

}
float testAlarm::testInherent()
{
	Alarma alr;
	alr.setaVF(45.7f);
	return alr.getaVF();
}


std::vector<std::string> testAlarm::testMensage() 
{
	Alarma alr;
	std::vector<std::string> mensajes;
	for (int i=0;i<3;i++) 
	{
		std::string mensa = "mensaje----" + std::to_string(i);
		mensajes.push_back(mensa);
	}
	alr.setMensajes(mensajes);
	return alr.getMensajes();
}

TEST(AlarmatestHerencias,testAvf) 
{
	testAlarm testa;
	ASSERT_EQ(45.7f,testa.testInherent());
}

TEST(AlarmatestMensaje,testmesaje)
{
	testAlarm testa;
	std::vector<std::string> vect = testa.testMensage();
	for (int i = 0; i < vect.size()-1; i++)
	{
		ASSERT_EQ("mensaje----"+std::to_string(i),vect.at(i));
	}
}