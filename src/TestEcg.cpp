#include "TestEcg.h"




TestEcg::TestEcg() {}


TestEcg::~TestEcg() {}

float TestEcg::testloadParam(int typeparm)
{
	ECG ecg;
	switch (typeparm)
	{
	case 0:
		ecg.setaVF(this->pram);
		return ecg.getaVF();
	break;

	case 1:
		ecg.setaVL(this->pram);
		return ecg.getaVL();
	break;

	case 2:
		ecg.setaVR(this->pram);
		return ecg.getaVR();
	break;

	case 3:
		ecg.setCVP(this->pram);
		return ecg.getCVP();
	break;

	case 4:
		ecg.setI(this->pram);
		return ecg.getI();
	break;

	case 5:
		ecg.setII(this->pram);
		return ecg.getII();
	break;

	case 6:
		ecg.setV(this->pram);
		return ecg.getV();
	break;

	case 7:
		ecg.setFre_Card(this->pram);
		return ecg.getFre_Card();
	break;

	

	default:
		
		break;
	}
	
	return 0.0f;
}


float TestEcg::getpram() 
{
	return this->pram;
}

void TestEcg::setpram(float par) 
{
	this->pram = par;
}

TEST(loadAVF, testloadParamAVF)
{
	TestEcg test;
	test.setpram(5.4f);
	ASSERT_EQ(5.4f,test.testloadParam(0));
}

TEST(loadAVL,testLoadParamAVL) 
{
	TestEcg test;
	test.setpram(11.4f);
	ASSERT_EQ(11.4f, test.testloadParam(1));
}

TEST(loadAVR, testLoadParamAVR)
{
	TestEcg test;
	test.setpram(12.4f);
	ASSERT_EQ(12.4f, test.testloadParam(2));
}

TEST(loadAVR, testLoadParamAVRCero)
{
	TestEcg test;
	test.setpram(0);
	ASSERT_EQ(0, test.testloadParam(2));
}


TEST(loadCVP, testLoadParamCVP)
{
	TestEcg test;
	test.setpram(18.4f);
	ASSERT_EQ(18.4f, test.testloadParam(3));
}


TEST(loadI,testLoadI) 
{
	TestEcg test;
	test.setpram(20.5f);
	ASSERT_EQ(20.5f,test.testloadParam(4));
}


TEST(loadII, testLoadII)
{
	TestEcg test;
	test.setpram(21.3f);
	ASSERT_EQ(21.3f, test.testloadParam(5));
}


TEST(loadV, testloadV)
{
	TestEcg test;
	test.setpram(45.4f);
	ASSERT_EQ(45.4f,test.testloadParam(6));
}


TEST(loadFrecRes,testFrecresp) 
{
	TestEcg test;
	test.setpram(18.7f);
	ASSERT_EQ(18.7f,test.testloadParam(7));
}