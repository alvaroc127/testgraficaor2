#if !defined(_TESTSPO2_)
#define _TESTSPO2_
#pragma once
#include "../Include/SPO2.h"
#include "gtest/gtest.h"

class testSPO2
{

public:
	float param;

	testSPO2();

	~testSPO2();
	
	float testParamSPO2(int );

};
#endif