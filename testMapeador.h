#if !defined (_TESTMAP_)
#define _TESTMAP_
#pragma once
#include <iostream>
#include <common/HL7Exception.h>
#include <ofLog.h>
#include <common/ObjToPipe.h>
#include <2.4/message/ORU_R01.h>
#include <ECG.h>
#include <Alarma.h>
#include <SignalYellowRed.h>
#include <FrecResp.h>
#include <SPO2.h>
#include <Temperatura.h>
#include <comunicacion/MapeadorHl7.h>
#include "gtest/gtest.h"

class testMapeador
{
	public:	

	testMapeador();

	~testMapeador();



	void testPipetoMessage();

	void testPipetoSeg();

	void testSplitMessage();


	ECG testMapEcgObj();

	Alarma testMapAlarmObj();

	SigYellRed testMapSigYellRedArt();

	SigYellRed testMapSigYellRedAP();


	FrecResp testMapSigFrecResp();


	SPO2 testMapSigSPO2();


	Temperatura testMapSigTemp();


	MapeadorHl7 testAllTestLoad();



};
#endif